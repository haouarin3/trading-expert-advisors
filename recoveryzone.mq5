//+------------------------------------------------------------------+
//|                                                  RecoveryZone.mq5|
//|                                               Haouari Noureddine |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Haouari Noureddine"
#property link      "https://t.me/joinchat/Mj8ZLBO9x56DZgN2vIwq7w"
#property version   "1.0.4"
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
#include <Trade\Trade.mqh>
#include <Trade\PositionInfo.mqh>

#define MA_MAGIC 754887129
int ExtHandle = 0;
bool ExtHedging = false;
CTrade ExtTrade;
CPositionInfo myposition;

enum Modes
{
PredefinedValues,
ArithmeticProgression
};

input double MinPowerToOpen  = 99;
input double volume = 0.1;
input int TargetProfit = 50;



input double STOPLOSS = 0;// STOPLOSS by position (PIPs)
input double TAKEPROFIT = 0;// Take profit by position (PIPs)
input int MaxNumberOfPositions = 7;

input double STOPLOSSTOTAL = 0.05;//STOPLOSSTOTAL % (0->1)
input double TAKEPROFITTOTAL = 0.1;//TAKEPROFITTOTAL % (0->1)
input double minAdx=25;
input double ZoneSize=50;
input double ZoneSizeStep=1;
input double MaxPricePoints=150;

input Modes mode=PredefinedValues;
input double step=150;

input double volume1 = 1;//TAKEPROFITTOTAL % (0->1)
input double volume2 = 1.4;//TAKEPROFITTOTAL % (0->1)
input double volume3 = 0.93;//TAKEPROFITTOTAL % (0->1)
input double volume4 = 1.24;//TAKEPROFITTOTAL % (0->1)
input double volume5 = 1.66;//TAKEPROFITTOTAL % (0->1)
input double volume6 = 2.21;//TAKEPROFITTOTAL % (0->1)
input double volume7 = 2.94;//TAKEPROFITTOTAL % (0->1)
input double volume8 = 3.1;//TAKEPROFITTOTAL % (0->1)

// v2=v1*(lastzonesize+lastspread+pointsgoal)

double volumes[8];
int m_nLastBars=0;
int indicators12_handler=0;
int indicators12_handler_1hours=0;

int adx_handler=0;

int PositionIndex=0;
int LastPosition = 0; 
double price1=0;
double price2=0;


double MaxPrice=0;
double MinPrice=0;

int LastSpread= 2;
double LastZoneSize = ZoneSize;
double LastVolume = volume;

int OnInit()
  {
   volumes[0]=volume1;
   volumes[1]=volume2;
   volumes[2]=volume3;
   volumes[3]=volume4;
   volumes[4]=volume5;
   volumes[5]=volume6;
   volumes[6]=volume7;
   volumes[7]=volume8;
   
   ExtHedging = ((ENUM_ACCOUNT_MARGIN_MODE) AccountInfoInteger(ACCOUNT_MARGIN_MODE) == ACCOUNT_MARGIN_MODE_RETAIL_HEDGING);
   ExtTrade.SetMarginMode();
   ExtTrade.SetTypeFillingBySymbol(Symbol());
//---
   indicators12_handler=iCustom(_Symbol, PERIOD_D1,"..\\Experts\\trading-expert-advisors\\12movingAverages");
   indicators12_handler_1hours=iCustom(_Symbol, PERIOD_H1,"..\\Experts\\trading-expert-advisors\\12movingAverages");
   adx_handler = iADX(_Symbol, PERIOD_D1, 7);
//---
   
         ObjectCreate(ChartID(),"price1",OBJ_HLINE,0,0,0);
         ObjectSetString(ChartID(),"price1",OBJPROP_TOOLTIP,"price1") ;
         
         ObjectCreate(ChartID(),"price2",OBJ_HLINE,0,0,0) ;
         ObjectSetString(ChartID(),"price2",OBJPROP_TOOLTIP,"price2") ;
         
          ObjectCreate(ChartID(),"MaxPrice",OBJ_HLINE,0,0,0) ;
          ObjectSetString(ChartID(),"MaxPrice",OBJPROP_TOOLTIP,"MaxPrice") ;
            
          ObjectCreate(ChartID(),"MinPrice",OBJ_HLINE,0,0,0);
          ObjectSetString(ChartID(),"MinPrice",OBJPROP_TOOLTIP,"MinPrice") ; 
   
   
   
   return(INIT_SUCCEEDED);
   
  }
  
  
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---

ObjectsDeleteAll(0);
  }
  
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
 // if (isThereANewCandel()) {
    CheckForClose();
    CheckForOpen();

  //  }
  }



//+------------------------------------------------------------------+
//| Close position by  indicatorIndex                              |
//+------------------------------------------------------------------+

void CheckForClose(){

  double powerDaily=getValue(12,1,indicators12_handler); 
 // Print("1PowerDaily = ",powerDaily);
  
    double power1Hours=getValue(12,1,indicators12_handler_1hours); 
//  Print("1Power1Hours = ",power1Hours);
  
  double balance = AccountInfoDouble(ACCOUNT_BALANCE);
   double Bid_Price = SymbolInfoDouble(_Symbol, SYMBOL_BID);
   double Ask_Price = SymbolInfoDouble(_Symbol, SYMBOL_ASK); 
  

   //
  if(((GetTotalProfit(MA_MAGIC)>-1*STOPLOSSTOTAL*balance)&&(Bid_Price>=MaxPrice || Ask_Price>=MaxPrice || Bid_Price<=MinPrice || Ask_Price<=MinPrice)|| (TAKEPROFITTOTAL!=0&&GetTotalProfit(MA_MAGIC)>=TAKEPROFITTOTAL*balance) ||  (STOPLOSSTOTAL!=0&&GetTotalProfit(MA_MAGIC)<= -1*STOPLOSSTOTAL*balance))) {
    
     Print("=======================================================================================================");
     ClosePosition();
     double balance2 = AccountInfoDouble(ACCOUNT_BALANCE);
   
    
   if(balance2<balance){
   Print("=>Bid_Price= ",Bid_Price,"| Ask_Price = ",Ask_Price,"| MinPrice = ",MinPrice,"| MaxPrice = ",MaxPrice,"| balance = ",balance,"| balance2 = ",balance2);

    Print("==========================================> WHAT! <====================================================");
   }
   
  
  }        
  
}


void CheckForOpen(){
    
    double adx[];
    double pdi[];
    double mdi[];
    if (CopyBuffer(adx_handler, 0, 0, 1, adx) != 1 || CopyBuffer(adx_handler, 1, 0, 1, pdi) != 1 || CopyBuffer(adx_handler, 2, 0, 1, mdi) != 1) {
       Print("CopyBuffer from ahc failed, no data");
       return;
    }

   
   double Bid_Price = SymbolInfoDouble(_Symbol, SYMBOL_BID);

   double Ask_Price = SymbolInfoDouble(_Symbol, SYMBOL_ASK); 
  
    
    double powerDaily=getValue(12,1,indicators12_handler); 
  //Print("PowerDaily = ",powerDaily);
  
    double power1Hours=getValue(12,1,indicators12_handler_1hours); 
  //Print("Power1Hours = ",power1Hours);
  
    //Print("==> CheckForOpen ind=",indicatorslist[indicatorIndex]);
    double balance = AccountInfoDouble(ACCOUNT_BALANCE);
     
    int ZoneSize2= ZoneSize+ZoneSizeStep*NumberOfPositions(); 
     
    if ((adx[0]>minAdx)&&(MaxNumberOfPositions==0 || NumberOfPositions()<=MaxNumberOfPositions)&&(MathAbs(powerDaily)>=MinPowerToOpen)&&(MathAbs(power1Hours)>=MinPowerToOpen)) { // Result != 0 not wait
     
      if(PositionIndex==0){  
       
         if(powerDaily>0){
         OpenPositon(1);
         LastPosition=1;
         price1=Ask_Price;
         price2=price1-ZoneSize2*_Point*10;
         
         MaxPrice = price1+MaxPricePoints*_Point*10;
         MinPrice = price2-MaxPricePoints*_Point*10;
         
         } else if(powerDaily<0) {
         
         price2=Bid_Price; 
         price1=price2+ZoneSize2*_Point*10;
         
         MaxPrice = price1+MaxPricePoints*_Point*10;
         MinPrice = price2-MaxPricePoints*_Point*10;
         
         OpenPositon(-1);
         LastPosition=-1;
         
         }
      
  
        } else if(LastPosition==1&&Bid_Price<price2 || LastPosition==-1&&Ask_Price>price1){
         LastPosition=LastPosition*-1;
       
         OpenPositon(LastPosition);
         
    
         price1=price1+ZoneSizeStep*NumberOfPositions()*_Point*10;
         price2=price2-ZoneSizeStep*NumberOfPositions()*_Point*10;
         MaxPrice = price1+MaxPricePoints*_Point*10;
         MinPrice = price2-MaxPricePoints*_Point*10;
      
        } 
        
        
    
    }
    
         // Print("PositionIndex = 0 ==> ",Bid_Price,"| Ask_Price = ",Ask_Price,"| MinPrice = ",MinPrice,"| MaxPrice = ",MaxPrice);  
         // Print("PositionIndex = 0 ==> price1= ",price1,"| price2 = ",price2);  
         
          ObjectSetDouble(ChartID(),"price1",OBJPROP_PRICE,price1);
          ObjectSetInteger(ChartID(),"price1",OBJPROP_COLOR,clrBeige);
          ObjectSetInteger(ChartID(),"price1",OBJPROP_STYLE, STYLE_SOLID);
          ObjectSetInteger(ChartID(),"price1",OBJPROP_WIDTH,4);
          
          ObjectSetDouble(ChartID(),"price2",OBJPROP_PRICE,price2);
          ObjectSetInteger(ChartID(),"price2",OBJPROP_COLOR,clrCyan);
          ObjectSetInteger(ChartID(),"price2",OBJPROP_STYLE,STYLE_SOLID);
          ObjectSetInteger(ChartID(),"price2",OBJPROP_WIDTH,4);
          
          ObjectSetDouble(ChartID(),"MaxPrice",OBJPROP_PRICE,MaxPrice);
          ObjectSetInteger(ChartID(),"MaxPrice",OBJPROP_COLOR,clrBeige);
          ObjectSetInteger(ChartID(),"MaxPrice",OBJPROP_STYLE,STYLE_SOLID);
          ObjectSetInteger(ChartID(),"MaxPrice",OBJPROP_WIDTH,6);
          
          ObjectSetDouble(ChartID(),"MinPrice",OBJPROP_PRICE,MinPrice);
          ObjectSetInteger(ChartID(),"MinPrice",OBJPROP_COLOR,clrCyan);
          ObjectSetInteger(ChartID(),"MinPrice",OBJPROP_STYLE,STYLE_SOLID);
          ObjectSetInteger(ChartID(),"MinPrice",OBJPROP_WIDTH,6);
       LastSpread= SymbolInfoInteger(Symbol(),SYMBOL_SPREAD);
       LastZoneSize=ZoneSize2;
}


//+------------------------------------------------------------------+
//| Open position by  indicatorIndex with  sig = -1 sell , 1 buy                               |
//+------------------------------------------------------------------+
void OpenPositon(int sig) {
  
  MqlTick tick; // Structure to get the latest prices      
  SymbolInfoTick(Symbol(), tick); // Assign current prices to structure 


  ENUM_ORDER_TYPE signal = WRONG_VALUE;
     double tp=0;
     double sl=0;
     
 if (sig == 1) {
    signal = ORDER_TYPE_BUY; // buy conditions
    if(STOPLOSS!=0){
       sl = NormalizeDouble(tick.bid - STOPLOSS * _Point * 10,_Digits);
      } 
      
    if(TAKEPROFIT!=0){
       tp = NormalizeDouble(tick.bid + TAKEPROFIT * _Point* 10,_Digits);
      } 
  } else if (sig == -1) {
    signal = ORDER_TYPE_SELL; // sell conditions
   
   
    if(STOPLOSS!=0){
       sl = NormalizeDouble(tick.ask + STOPLOSS * _Point * 10,_Digits) ;
      } 
      
    if(TAKEPROFIT!=0){
       tp = NormalizeDouble(tick.ask - TAKEPROFIT * _Point * 10,_Digits) ;
      } 

  } else {
   return;
    Alert("Signal not considered");
  }
  
  long magicNumber=  MA_MAGIC;  
  ExtTrade.SetExpertMagicNumber(magicNumber);
  LastVolume = getVolume(PositionIndex);
  ExtTrade.PositionOpen(_Symbol, signal,LastVolume,
  SymbolInfoDouble(_Symbol, signal == ORDER_TYPE_SELL ? SYMBOL_BID : SYMBOL_ASK),sl, tp,"ZoneRecovery");
  

  

  if (ExtTrade.ResultRetcode() == 10008 || ExtTrade.ResultRetcode() == 10009) //Request is completed or order placed
  {
      Print("====| Open Position" );
     PositionIndex++;
   //Print("==> Open Indicator = ",indicatorslist[indicatorIndex]);
   
   // OpenPendingPositon(PendingOrderSignal, volume,price,tp,0,"ORDER_TYPE_BUY_STOP",indicatorIndex);
    
  } else {
    //Print("==> !OK ", "==> Open Indicator = ",indicatorslist[indicatorIndex], ", err = ",GetLastError());
    ResetLastError();
    return;
  }

}

double getVolume(int PositionIndex){

/*if(mode==PredefinedValues) {
   //return volumes[PositionIndex];
} else {*/


 //return volume*MathRound(MathPow(step,NumberOfPositions())); 


if(NumberOfPositions() == 0) {
LastVolume = volume;
return volume;
} else {

double coveringVolume= LastVolume*(LastZoneSize+LastSpread/10+SymbolInfoInteger(Symbol(),SYMBOL_SPREAD)/10)/(double) MaxPricePoints ;
double profitVolume= TargetProfit/((double) MaxPricePoints*10);

double NewVolume = MathCeil(100*(coveringVolume+profitVolume))/100;

if(LastVolume <= NewVolume){
   NewVolume+=0.01;
}
Print("******> "," |  LastSpread = ",LastSpread, "|  Spread = ",SymbolInfoInteger(Symbol(),SYMBOL_SPREAD), "coveringVolume = ",coveringVolume, " |  profitVolume = ",profitVolume," | NewVolume = ",NewVolume," | LastVolume = ",LastVolume, " | MaxPricePoints = ",MaxPricePoints, " | LastSpread = ",LastSpread, " | LastZoneSize = ",LastZoneSize);
return NewVolume;
}

}


void ClosePosition(){
 int magicNumber=  MA_MAGIC;  
 for(int i=PositionsTotal()-1; i>=0; i--)
    {
       ulong ticket=PositionGetTicket(i);
       PositionSelectByTicket(ticket);
       //Print("==> Magic number of closed deal=",PositionGetInteger(POSITION_MAGIC)," | indicatorIndex = ",indicatorIndex);
       if(PositionGetString(POSITION_SYMBOL)==Symbol()&&PositionGetInteger(POSITION_MAGIC)==magicNumber){
         Print("=======> Close");  
          ExtTrade.PositionClose(ticket);   
         }
    }  
    
  PositionIndex=0;
  LastPosition=0;
 
 /*for(int i=OrdersTotal()-1;i>=0;i--)
     {
      ulong ticket=OrderGetTicket(i);
      OrderSelect(ticket);

      if(OrderGetString(ORDER_SYMBOL)==Symbol()&&OrderGetInteger(ORDER_MAGIC)==magicNumber)
        { 
         
          ExtTrade.OrderDelete(ticket);
        }
     }*/ 
}


int NumberOfPositions()
{
  int c=0;
  int magicNumber=  MA_MAGIC  ;
  for(int i=PositionsTotal()-1; i>=0; i--)
    {
       ulong ticket=PositionGetTicket(i);
       PositionSelectByTicket(ticket);
        if(PositionGetString(POSITION_SYMBOL)==Symbol()&&PositionGetInteger(POSITION_MAGIC)==magicNumber){ 
            c++;
         }
    } 
  return(c);
}



//+------------------------------------------------------------------+
//| Get value from indicator buffer                                     |
//+------------------------------------------------------------------+
int getValue(int indicatorIndex,int start = 0,int handler=0){
        double  value[];
        if (CopyBuffer(handler,indicatorIndex, 0, 3, value) != 3) {
          Print("CopyBuffer from indicators12 failed, no data");
          return 0;
        } 
        ArraySetAsSeries(value, true);
 return (int)value[start];
}


//+------------------------------------------------------------------+
//| isThereANewCandel ? True or False                                   |
//+------------------------------------------------------------------+
bool isThereANewCandel() {
  bool m_bNewBar = false;
  int nBars = Bars(Symbol(), _Period);
  if (m_nLastBars != nBars) {
    m_nLastBars = nBars;
    m_bNewBar = true;
  }
  return m_bNewBar;
}


double GetTotalProfit(long Magic){
   double totalProfit = 0; 
   for(int i=PositionsTotal()-1; i>=0; i--)
    {
       ulong ticket=PositionGetTicket(i);
       PositionSelectByTicket(ticket);
       if(PositionGetString(POSITION_SYMBOL)==Symbol()&&PositionGetInteger(POSITION_MAGIC)==Magic){
          totalProfit+=PositionGetDouble(POSITION_PROFIT);
         }
    }  
   return totalProfit;
}
