//+------------------------------------------------------------------+
//|                                              HighLowIndicator.mq5 |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright   "Noureddine Haouari"
#property link        "http://bit.ly/34hqMu8"
#property description "HighLow by Noureddine Haouari "
#property version "1.0.1"

//--- indicator settings
#property indicator_chart_window
#property indicator_buffers 3
#property indicator_plots   2
#property indicator_type1   DRAW_FILLING
#property indicator_type2   DRAW_LINE
#property indicator_color1  DodgerBlue,Gray
#property indicator_color2  Blue
#property indicator_label1  "Channel upper;Channel lower"
#property indicator_label2  "Channel median"

//--- indicator buffers
double    ExtHighBuffer[];
double    ExtLowBuffer[];
double    ExtMiddBuffer[];
int correct = 0;
int wrong = 0;
int m_nLastBars =0;
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
void OnInit()
  {
//--- indicator buffers mapping
   SetIndexBuffer(0,ExtHighBuffer,INDICATOR_DATA);
   SetIndexBuffer(1,ExtLowBuffer,INDICATOR_DATA);
   SetIndexBuffer(2,ExtMiddBuffer,INDICATOR_DATA);
//--- set accuracy
   IndicatorSetInteger(INDICATOR_DIGITS,_Digits);
//--- set first bar from what index will be drawn
   PlotIndexSetInteger(0,PLOT_DRAW_BEGIN,0);
//---- line shifts when drawing
   PlotIndexSetInteger(0,PLOT_SHIFT,1);
   PlotIndexSetInteger(1,PLOT_SHIFT,1);
//--- name for DataWindow and indicator label
   IndicatorSetString(INDICATOR_SHORTNAME,"Price Channel");
   PlotIndexSetString(0,PLOT_LABEL,"High Low ");
   PlotIndexSetString(1,PLOT_LABEL,"Median");
//--- set drawing line empty value
   PlotIndexSetDouble(0,PLOT_EMPTY_VALUE,0.0);
   PlotIndexSetDouble(1,PLOT_EMPTY_VALUE,0.0);
//--- initialization done
  }
  
  

  
//+------------------------------------------------------------------+
//| get highest value for range                                      |
//+------------------------------------------------------------------+
double Highest(double close,double low)
  {
   double res=0.01525+(1.0875*close)+(-0.09728*low);
  
   return(res);
  }
//+------------------------------------------------------------------+
//| get lowest value for range                                       |
//+------------------------------------------------------------------+
double Lowest(double close,double low)
  {
   double res=-0.0020+(0.8181*close)+(0.1803*low);

   return(res);
  }
//+------------------------------------------------------------------+
//| Price Channell                                                   |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
  
  int limit;
   if(prev_calculated<1)
      limit=0;
   else limit=prev_calculated-1;
   
   
   for(int i=limit;i<rates_total && !IsStopped();i++)
     {
      if(i!=0&&((high[i]> ExtHighBuffer[i-1])||(low[i]< ExtLowBuffer[i-1]))){
         wrong++;
      } else if(i!=0){
         correct++;
      }
     
      ExtHighBuffer[i]=Highest(close[i],low[i]);
      ExtLowBuffer[i]=Lowest(close[i],low[i]);
      ExtMiddBuffer[i]=(ExtHighBuffer[i]+ExtLowBuffer[i])/2.0;
     }
     
     
     double succ= 100*(double)correct/(double)(correct+wrong);
     Comment("Success Rate == ", NormalizeDouble(succ,2),"%");
     Print("Success Rate == ", NormalizeDouble(succ,2),"%");
//--- OnCalculate done. Return new prev_calculated.
   return(rates_total);
  }
//+------------------------------------------------------------------+
