//+------------------------------------------------------------------+
//|                                                 exportValues.mq5 |
//|                                               HAOUARI NOureddine |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "HAOUARI NOureddine"
#property link      "https://www.mql5.com"
#property version   "1.0.1"
input int    IndicatorPeriod=14;
input string Indicator_Directory_And_Name="Examples\\RSI";
input int Indicator_buffer=0;
input int NumberOfCandels=100;

int m_nLastBars=0;

int OnInit()
  {
 
   return(INIT_SUCCEEDED);
  }
bool isThereANewCandel() {

  bool m_bNewBar = false;
  int nBars = Bars(Symbol(), PERIOD_CURRENT);
  if (m_nLastBars != nBars) {
    m_nLastBars = nBars;
    m_bNewBar = true;
  }
  return m_bNewBar;

}

  
void OnTick(){
   if(isThereANewCandel()){
        UpdateFile();
   }
} 
 

void UpdateFile(){
  
   MqlRates  rates_array[];
   string sSymbol=Symbol();

// Convert Period to string to use it in the file name
   string  sPeriod=EnumToString(Period());
// Comment to appear in the up left screen
   Comment("Exporting ... Please wait... ");

// Prepare file name, e.g: EURUSD_PERIOD_H1(RSI,14)
   string       ExtFileName; // ="XXXXXX_PERIOD_H1(RSI,14).CSV";
   ExtFileName=sSymbol;
   int pos=StringFind(Indicator_Directory_And_Name,"\\",0);
   string indicatorName=StringSubstr(Indicator_Directory_And_Name,pos+1,-1);
   string indicatorPeriod=IntegerToString(IndicatorPeriod);
   string IndicatorBufferIndice=IntegerToString(Indicator_buffer);
   StringConcatenate(ExtFileName,sSymbol,"_",sPeriod,"(",indicatorName,",",indicatorPeriod,",",IndicatorBufferIndice,")",".CSV");

   ArraySetAsSeries(rates_array,true);
   int MaxBar=TerminalInfoInteger(TERMINAL_MAXBARS);
   int iCurrent=CopyRates(sSymbol,Period(),0,MaxBar,rates_array);

   double IndicatorBuffer[];
   SetIndexBuffer(0,IndicatorBuffer,INDICATOR_DATA);

   int bars=Bars(sSymbol,PERIOD_CURRENT);
   int to_copy=bars;

   int rsiHandle=iCustom(sSymbol,PERIOD_CURRENT,Indicator_Directory_And_Name,IndicatorPeriod);       // Change here.

   CopyBuffer(rsiHandle,Indicator_buffer,0,to_copy,IndicatorBuffer);
   ArraySetAsSeries(IndicatorBuffer,true);

   int fileHandle=FileOpen(ExtFileName,FILE_WRITE|FILE_CSV);
   string header="Date,Time,Open,High,Low,Close,IND \n";
   FileWriteString(fileHandle,header);
   
   for(int i=0; i<NumberOfCandels-1; i++)
     {
      string outputData=StringFormat("%s",TimeToString(rates_array[i].time,TIME_DATE));
      outputData+=","+TimeToString(rates_array[i].time,TIME_MINUTES);
      outputData+=","+ DoubleToString(rates_array[i].open,4);
      outputData+=","+ DoubleToString(rates_array[i].high,4);
      outputData+=","+ DoubleToString(rates_array[i].low,4);
      outputData+=","+ DoubleToString(rates_array[i].close,4);
      outputData+=","+ DoubleToString(IndicatorBuffer[i],2);
      outputData+="\n";
      FileWriteString(fileHandle,outputData);
     }

   FileClose(fileHandle);
   Comment("Exported Successfully");


}
  