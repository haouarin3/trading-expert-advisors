//+------------------------------------------------------------------+
//|                                           12indicatorsExpert.mq5 |
//|                                               Haouari Noureddine |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Haouari Noureddine"
#property link      "https://t.me/joinchat/Mj8ZLBO9x56DZgN2vIwq7w"
#property version   "1.0.3"
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
#include <Trade\Trade.mqh>
#include <Trade\PositionInfo.mqh>

#define MA_MAGIC 92524577
int ExtHandle = 0;
bool ExtHedging = false;
CTrade ExtTrade;
CPositionInfo myposition;

input double MinPower  = 20;
input double volume = 0.1;
input double STOPLOSS = 0;
input double TAKEPROFIT = 0;


input int max_period_number = 200;
input bool enhancedPowerCalculation = false;// 
input double historicalFactor= 0; 
input double instantaneousFactor= 1; 

input int MA1_Period = 10; // Moving Average period 01
input int MA2_Period = 30; // Moving Average period 02
input int MA3_Period = 30; // Moving Average period 03
input int MA4_Period = 40; // Moving Average period 04
input int MA5_Period = 50; // Moving Average period 05
input int MA6_Period = 60; // Moving Average period 06
input int MA7_Period = 70; // Moving Average period 07
input int MA8_Period = 80; // Moving Average period 08
input int MA9_Period = 90; // Moving Average period 09
input int MA10_Period = 100; // Moving Average period 10
input int MA11_Period = 150; // Moving Average period 11
input int MA12_Period = 200; // Moving Average period 12



input bool KeepOpened=true;// keep positions opened   
input bool IND1=true;
input bool IND2=true;
input bool IND3=true;
input bool IND4=true;
input bool IND5=true;
input bool IND6=true;
input bool IND7=true;
input bool IND8=true;
input bool IND9=true;
input bool IND10=true;
input bool IND11=true;
input bool IND12=true;


int USE[12];

int m_nLastBars=0;
int indicators12_handler=0;
bool firstTime=true;
string indicatorslist[12] = {
  "IND1",
  "IND2",
  "IND3",
  "IND4",
  "IND5",
  "IND6",
  "IND7",
  "IND8",
  "IND9",
  "IND10",
  "IND11",
  "IND12"
};

int OnInit()
  {
 
    USE[0] = IND1;
    USE[1] = IND2;
    USE[2] = IND3;
    USE[3] = IND4;
    USE[4] = IND5;
    USE[5] = IND6;
    USE[6] = IND7;
    USE[7] = IND8;
    USE[8] = IND9;
    USE[9] = IND10;
    USE[10] = IND11;
    USE[11] = IND12;
  
    ExtHedging = ((ENUM_ACCOUNT_MARGIN_MODE) AccountInfoInteger(ACCOUNT_MARGIN_MODE) == ACCOUNT_MARGIN_MODE_RETAIL_HEDGING);
    ExtTrade.SetMarginMode();
    ExtTrade.SetTypeFillingBySymbol(Symbol());
//---
   indicators12_handler=iCustom(_Symbol, _Period,"..\\Experts\\trading-expert-advisors\\12movingAverages",max_period_number, enhancedPowerCalculation, historicalFactor, instantaneousFactor,MA1_Period,MA2_Period,MA3_Period,MA4_Period,MA5_Period,MA6_Period,MA7_Period,MA8_Period,MA9_Period,MA10_Period,MA11_Period,MA12_Period);//12movingAverages,12indicators
//---
   return(INIT_SUCCEEDED); 
  }
  
  
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---

ObjectsDeleteAll(0);
  }
  
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
  if (isThereANewCandel()) {
    
    int results[12];
    int preResult[12];
    calculate(results,1);
    calculate(preResult,2);
    for (int i = 0; i < 12; i++) {
        if(firstTime){
         CheckForOpen(results[i],preResult[i],i,results);
        } else if(CheckForClose(results[i],preResult[i],i)) {
            CheckForOpen(results[i],preResult[i],i,results);
         }
      } 
    ShowProfits();     
    }
    firstTime=false;
   
  }



//+------------------------------------------------------------------+
//| Close position by  indicatorIndex                              |
//+------------------------------------------------------------------+

bool CheckForClose(int Result,int PrivousResult,int indicatorIndex){
 
  bool close=false;
 
  if (Result != PrivousResult&&((PrivousResult==1&&NumberOfPositions(indicatorIndex,POSITION_TYPE_BUY)==1)||(PrivousResult==-1&&NumberOfPositions(indicatorIndex,POSITION_TYPE_SELL)==1))) {
     
     //Print("***",indicatorslist[indicatorIndex],"Result =",Result," | Previous Result = ",PrivousResult);
     ClosePosition(indicatorIndex);
     close=true;
    } else if(Result != PrivousResult)
    {
     close=true;
    
    } else if(Result == PrivousResult&&KeepOpened&&((PrivousResult==1&&NumberOfPositions(indicatorIndex,POSITION_TYPE_BUY)==0) || (PrivousResult==-1&&NumberOfPositions(indicatorIndex,POSITION_TYPE_SELL)==0))){
      // Print(indicatorslist[indicatorIndex]," Result =",Result," | Previous Result = ",PrivousResult);
       close=true;
    } else {
      //  Print(indicatorslist[indicatorIndex]," Result =",Result," | Previous Result = ",PrivousResult);
    }
    
          
  return close;
}


void CheckForOpen(int Result,int PrivousResult,int indicatorIndex,int & results[]){
    
    /*MqlRates rt[2];
     if (CopyRates(_Symbol, _Period, 0, 2, rt) != 2) {
       Print("CopyRates of ", _Symbol, " failed, no history");
       return;
     }
    ArraySetAsSeries(rt, true);
    
    
    
    bool lastCandelFollowTrend = ((rt[0].close>rt[0].open&&Result==1) ||(rt[0].open>rt[0].close&&Result==-1));
   */
    //Print("==> CheckForOpen ind=",indicatorslist[indicatorIndex]);
    
  
    double power=getValue(12,1);
    Print("Power = ",power);
       
    bool IndicatorFollowPower=true;
    if(MinPower!=0){  // 0 ignore this condition
     IndicatorFollowPower = (Result == -1&&power<=-1*MinPower)||(Result == 1&&power>=MinPower);
    }
     
    if (Result != 0&&IndicatorFollowPower) { // Result != 0 not wait
      OpenPositon(indicatorIndex,Result);
    }
}


//+------------------------------------------------------------------+
//| Get the signle of each indicator                                      |
//+------------------------------------------------------------------+
void calculate(int & results[],int start=0) {
 for (int i=0;i<12;i++){
   results[i]=getValue(i,start);
 }       
}





//+------------------------------------------------------------------+
//| Open position by  indicatorIndex with  sig = -1 sell , 1 buy                               |
//+------------------------------------------------------------------+
void OpenPositon(int indicatorIndex,int sig) {
  
  MqlTick tick; // Structure to get the latest prices      
  SymbolInfoTick(Symbol(), tick); // Assign current prices to structure 


  ENUM_ORDER_TYPE signal = WRONG_VALUE;
     double tp=0;
     double sl=0;
     
 if (sig == 1) {
    signal = ORDER_TYPE_BUY; // buy conditions
    if(STOPLOSS!=0){
       sl = NormalizeDouble(tick.bid - STOPLOSS * _Point * 10,_Digits);
      } 
      
    if(TAKEPROFIT!=0){
       tp = NormalizeDouble(tick.bid + TAKEPROFIT * _Point* 10,_Digits);
      } 
  } else if (sig == -1) {
    signal = ORDER_TYPE_SELL; // sell conditions
   
   
    if(STOPLOSS!=0){
       sl = NormalizeDouble(tick.ask + STOPLOSS * _Point * 10,_Digits) ;
      } 
      
    if(TAKEPROFIT!=0){
       tp = NormalizeDouble(tick.ask - TAKEPROFIT * _Point * 10,_Digits) ;
      } 

  } else {
   return;
    Alert("Signal not considered");
  }
  
  long magicNumber=  MA_MAGIC+ indicatorIndex;  
  ExtTrade.SetExpertMagicNumber(magicNumber);
  ExtTrade.PositionOpen(_Symbol, signal, volume,
    SymbolInfoDouble(_Symbol, signal == ORDER_TYPE_SELL ? SYMBOL_BID : SYMBOL_ASK),sl, tp, indicatorslist[indicatorIndex]);
  
  ENUM_ORDER_TYPE PendingOrderSignal = signal==ORDER_TYPE_SELL ? ORDER_TYPE_BUY_STOP : ORDER_TYPE_SELL_STOP;
  double price =sl;//
  
  

  if (ExtTrade.ResultRetcode() == 10008 || ExtTrade.ResultRetcode() == 10009) //Request is completed or order placed
  {
   
   //Print("==> Open Indicator = ",indicatorslist[indicatorIndex]);
   
   // OpenPendingPositon(PendingOrderSignal, volume,price,tp,0,"ORDER_TYPE_BUY_STOP",indicatorIndex);
    
  } else {
    //Print("==> !OK ", "==> Open Indicator = ",indicatorslist[indicatorIndex], ", err = ",GetLastError());
    ResetLastError();
    return;
  }

}

void ClosePosition(int indicatorIndex){
 int magicNumber=  MA_MAGIC+ indicatorIndex;  
 for(int i=PositionsTotal()-1; i>=0; i--)
    {
       ulong ticket=PositionGetTicket(i);
       PositionSelectByTicket(ticket);
       //Print("==> Magic number of closed deal=",PositionGetInteger(POSITION_MAGIC)," | indicatorIndex = ",indicatorIndex);
       if(PositionGetString(POSITION_SYMBOL)==Symbol()&&PositionGetInteger(POSITION_MAGIC)==magicNumber){
         // Print("=======> Close");  
          ExtTrade.PositionClose(ticket);   
         }
    }  
 
 /*for(int i=OrdersTotal()-1;i>=0;i--)
     {
      ulong ticket=OrderGetTicket(i);
      OrderSelect(ticket);

      if(OrderGetString(ORDER_SYMBOL)==Symbol()&&OrderGetInteger(ORDER_MAGIC)==magicNumber)
        { 
         
          ExtTrade.OrderDelete(ticket);
        }
     }*/ 
}


int NumberOfPositions(int indicatorIndex,ENUM_POSITION_TYPE Type)
{
  int c=0;
  int magicNumber=  MA_MAGIC+ indicatorIndex;  
  for(int i=PositionsTotal()-1; i>=0; i--)
    {
       ulong ticket=PositionGetTicket(i);
       PositionSelectByTicket(ticket);
        if(PositionGetString(POSITION_SYMBOL)==Symbol()&&PositionGetInteger(POSITION_MAGIC)==magicNumber&&PositionGetInteger(POSITION_TYPE)==Type){ 
            c++;
         }
    } 
  return(c);
}



//+------------------------------------------------------------------+
//| Get value from indicator buffer                                     |
//+------------------------------------------------------------------+
int getValue(int indicatorIndex,int start = 0){
        double  value[];
        if (CopyBuffer(indicators12_handler,indicatorIndex, 0, 3, value) != 3) {
          Print("CopyBuffer from indicators12 failed, no data");
          return 0;
        } 
        ArraySetAsSeries(value, true);
 return (int)value[start];
}


//+------------------------------------------------------------------+
//| isThereANewCandel ? True or False                                   |
//+------------------------------------------------------------------+
bool isThereANewCandel() {
  bool m_bNewBar = false;
  int nBars = Bars(Symbol(), PERIOD_CURRENT);
  if (m_nLastBars != nBars) {
    m_nLastBars = nBars;
    m_bNewBar = true;
  }
  return m_bNewBar;
}


//+------------------------------------------------------------------+
//| Show profits of each indicator                                   |
//+------------------------------------------------------------------+
void ShowProfits(){
   Print("========================= \n");
   Print("******* Profits *********\n");
   Print("========================= \n");
   for(int i=0;i<12;i++){
      long magic = MA_MAGIC+i;
      Print(indicatorslist[i]," => ",GetProfit(magic));      
   }
   Print("========================= \n");
}



//+------------------------------------------------------------------+
//| getProfit of one strategy using its Magic number                                 |
//+------------------------------------------------------------------+
double GetProfit( long Magic )
{
 double Res = 0;
 if (HistorySelect(0, INT_MAX))
   for (int i = HistoryDealsTotal() - 1; i >= 0; i--)
   {
     const ulong Ticket = HistoryDealGetTicket(i);
     
     if((HistoryDealGetInteger(Ticket, DEAL_MAGIC) == Magic) && (HistoryDealGetString(Ticket, DEAL_SYMBOL) == Symbol()))
       Res += HistoryDealGetDouble(Ticket, DEAL_PROFIT);
   }
     
  return(Res);
}
