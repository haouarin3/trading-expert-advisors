//+############################------------+
//|                                                 PendingOrders.mq5 |
//|                        Copyright 2019, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+############################------------+
#property copyright "HAOUARI Noureddine"
#property link "https://www.mql5.com"
#property  version "1.05"
#property description "Pending Orders Expert Advisor"
//+############################------------+
//| Expert initialization function                                   |
//+############################------------+

#include <Trade\Trade.mqh>
#include <Trade\PositionInfo.mqh>
#include <ChartObjects\ChartObjectsTxtControls.mqh>

#define MAGIC_Long 9775277
#define MAGIC_Short 8975442

int ExtHandle = 0;
bool ExtHedging = false;
CTrade ExtTrade;
CPositionInfo myposition;
int m_nLastBars= 0;
//double volume[7]={0.12,0.14,0.16,0.18,0.20,0.22,0.24};
//double pricePips[7]={12,20,18,16,14,12,10};
//double stopLimitPips[7]={46,80,114,148,184,218,252};

input double auto= true;// automatic trading 
input double STOPLOSSTOTAL = 0.05;//STOPLOSSTOTAL % (0->1)
input double TAKEPROFITTOTAL = 0.1;//TAKEPROFITTOTAL % (0->1)
input double shortFactor = 0.1;//shortFactor (0->1)

input double  volume1=0.12;
input double  volume2=0.14;
input double  volume3=0.16;
input double  volume4=0.18;
input double  volume5=0.20;
input double  volume6=0.22;
input double  volume7=0.24;

input double  pricePips1=12;
input double  pricePips2=20;
input double  pricePips3=18;
input double  pricePips4=16;
input double  pricePips5=14;
input double  pricePips6=12;
input double  pricePips7=10;

input double  stopLimitPips1=46;
input double  stopLimitPips2=80;
input double  stopLimitPips3=114;
input double  stopLimitPips4=148;
input double  stopLimitPips5=184;
input double  stopLimitPips6=218;
input double  stopLimitPips7=252;

double volume[7];
double pricePips[7];
double stopLimitPips[7];



CChartObjectButton OpenOrders;
CChartObjectButton CloseAll;

CChartObjectButton OpenOrders2; // short 
CChartObjectButton CloseAll2;
int OnInit()
  {
  volume[0]=volume1;
  volume[1]=volume2;
  volume[2]=volume3;
  volume[3]=volume4;
  volume[4]=volume5;
  volume[5]=volume6;
  volume[6]=volume7;
  
  pricePips[0]=pricePips1;
  pricePips[1]=pricePips2;
  pricePips[2]=pricePips3;
  pricePips[3]=pricePips4;
  pricePips[4]=pricePips5;
  pricePips[5]=pricePips6;
  pricePips[6]=pricePips7;

  stopLimitPips[0]=stopLimitPips1;
  stopLimitPips[1]=stopLimitPips2;
  stopLimitPips[2]=stopLimitPips3;
  stopLimitPips[3]=stopLimitPips4;
  stopLimitPips[4]=stopLimitPips5;
  stopLimitPips[5]=stopLimitPips6;
  stopLimitPips[6]=stopLimitPips7;
 
 
   int   sy=10;
   int   dy=16;

//---
   sy+=100;
//--- creation Buttons
   OpenOrders.Create(0,"OpenPendingOrders",0,10,sy,180,20);
   OpenOrders.Description("Open Long Pending Orders");
   OpenOrders.Color(Red);
   OpenOrders.FontSize(8);
//---
   CloseAll.Create(0,"CloseAll",0,190,sy,150,20);
   CloseAll.Description("Close All Long");
   CloseAll.Color(Red);
   CloseAll.FontSize(8);
   
    sy+=40;
   //--- creation Buttons
   OpenOrders2.Create(0,"OpenPendingOrders2",0,10,sy,180,20);
   OpenOrders2.Description("Open Short Pending Orders");
   OpenOrders2.Color(Red);
   OpenOrders2.FontSize(8);
//---
   CloseAll2.Create(0,"CloseAll2",0,190,sy,150,20);
   CloseAll2.Description("Close All Short");
   CloseAll2.Color(Red);
   CloseAll2.FontSize(8);
//---
//---
if(auto){
   OpenPendingOrders(MAGIC_Long,false);
   OpenPendingOrders(MAGIC_Short,true);
  }
   return(INIT_SUCCEEDED);
  }
  
  
double maxtickwithoutMove= 100;
double maxDifferenceCounted= 0.1;
double tickwithoutMove=0;
double profit = 0;


//+------------------------------------------------------------------+
//| deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---

ObjectsDeleteAll(0);
  }
    
    
void OnTick() {

double longProfit = GetTotalProfit(MAGIC_Long);
double shortProfit = GetTotalProfit(MAGIC_Short);
double total =longProfit+ shortProfit;
if(MathAbs(total-profit)<0.1){
tickwithoutMove++;
}
double profit=total;
Comment(" Long Profit  =  ", longProfit ,"\n Short Profit= ",shortProfit,"\n Total Profit = ",total);
double balance = AccountInfoDouble(ACCOUNT_BALANCE);

if((total>=TAKEPROFITTOTAL*balance&&TAKEPROFITTOTAL!=0)||(total<=-1*STOPLOSSTOTAL*balance&&STOPLOSSTOTAL!=0) || isThereANewCandel()){
   CloseAllOrders(MAGIC_Long);
   CloseAllOrders(MAGIC_Short);
 if(auto){  
   OpenPendingOrders(MAGIC_Long,false);
   OpenPendingOrders(MAGIC_Short,true);
   }
  tickwithoutMove=0; 
   
}


CheckButtons();

 /*if(isThereANewCandel()){
   CloseAllOrders();
   for(int i=0;i<7;i++){
   OpenPositontest(i,ORDER_TYPE_BUY_STOP_LIMIT);
   OpenPositontest(i,ORDER_TYPE_SELL_STOP_LIMIT);
  }
 */
}


void OpenPendingOrders(long Magic, bool Short=false){

   for(int i=0;i<7;i++){
      OpenPositontest(i,ORDER_TYPE_BUY_STOP_LIMIT,Short,Magic);
      OpenPositontest(i,ORDER_TYPE_SELL_STOP_LIMIT,Short,Magic);
     }
     
}

void CheckButtons(void)
  {
  
   if(OpenOrders.State())
     {
      OpenOrders.State(false);
      OpenPendingOrders(MAGIC_Long,false);
     }
     
   if(CloseAll.State())
     {
      CloseAll.State(false);
      CloseAllOrders(MAGIC_Long);
      }
      
   if(OpenOrders2.State())
     {
      OpenOrders2.State(false);
      OpenPendingOrders(MAGIC_Short,true);
     }
     
   if(CloseAll2.State())
     {
      CloseAll2.State(false);
      CloseAllOrders(MAGIC_Short);
      }
  }
  


//+------------------------------------------------------------------+
//| Close all positions                             |
//+------------------------------------------------------------------+
void CloseAllOrders(long Magic){
 for(int i=PositionsTotal()-1; i>=0; i--)
    {
       ulong ticket=PositionGetTicket(i);
       PositionSelectByTicket(ticket);
       if(PositionGetString(POSITION_SYMBOL)==Symbol()&&PositionGetInteger(POSITION_MAGIC)==Magic){
          ExtTrade.PositionClose(ticket);   
         }
    }  
 
 for(int i=OrdersTotal()-1;i>=0;i--)
     {
      ulong ticket=OrderGetTicket(i);
      OrderSelect(ticket);
      
      if(OrderGetString(ORDER_SYMBOL)==Symbol()&&OrderGetInteger(ORDER_MAGIC)==Magic)
        { 
          ExtTrade.OrderDelete(ticket);
        }
     } 
}


double GetTotalProfit(long Magic){
   double totalProfit = 0; 
   for(int i=PositionsTotal()-1; i>=0; i--)
    {
       ulong ticket=PositionGetTicket(i);
       PositionSelectByTicket(ticket);
       if(PositionGetString(POSITION_SYMBOL)==Symbol()&&PositionGetInteger(POSITION_MAGIC)==Magic){
          totalProfit+=PositionGetDouble(POSITION_PROFIT);
         }
    }  
   return totalProfit;
}


void OpenPositontest(int index,ENUM_ORDER_TYPE signal,bool ShortFrame=false,long Magic=MAGIC_Long){//ENUM_ORDER_TYPE signal,
   MqlTick tick; // Structure to get the latest prices      
   SymbolInfoTick(Symbol(), tick); // Assign current prices to structure 
   
   double pipsFactor = 10; 
   
   if (ShortFrame) {
     pipsFactor=shortFactor*10; 
   }
   
   
   double factor = 1;
   double price= tick.ask;
   if(signal==ORDER_TYPE_SELL_STOP_LIMIT ){
      factor = -1;
      price= tick.bid;
   }
  
   
   int pips = 0;
   for(int i=0;i<=index;i++){
   pips+= pricePips[i];
   }
   
   double spread = 2*_Point*pipsFactor ;//MathAbs(tick.ask-tick.bid);
   double positionPrice =  price+factor*pips*_Point*pipsFactor+factor*(index+1)*spread;
   double stopLimit = positionPrice-factor*stopLimitPips[index]*_Point*pipsFactor-factor*spread;
   
   OpenPositon(signal,volume[index],positionPrice,stopLimit,0,0,"",Magic);
   
   Print("Pips= ",pips," | stopLimit = ", spread);
   Print("Price= ",positionPrice," | stopLimit = ", stopLimit);
}  
    
  
void OpenPositon( ENUM_ORDER_TYPE signal,double volume, double price,double stoplimit,double sl,double tp,string comment,long Magic){
//--- declare and initialize the trade request and result of trade request
   MqlTradeRequest request={0};
   MqlTradeResult  result={0};
//--- parameters of request
   request.action   =TRADE_ACTION_PENDING;                     // type of trade operation
   request.symbol   =Symbol();                              // symbol
   request.volume   =volume;                                   
   request.type     =signal;                        // order type
   request.price    =price; // price for opening
   request.stoplimit = stoplimit;
   request.deviation=5;                                     // allowed deviation from the price
   request.magic    =Magic;                          // MagicNumber of the order
   request.sl = sl;
   request.tp = tp;
   request.comment = comment;
//--- send the request
   if(!OrderSend(request,result))
      PrintFormat("OrderSend error %d",GetLastError());     // if unable to send the request, output the error code
//--- information about the operation
   PrintFormat("retcode=%u  deal=%I64u  order=%I64u",result.retcode,result.deal,result.order);
}

bool isThereANewCandel() {
  bool m_bNewBar = false;
  int nBars = Bars(Symbol(), PERIOD_CURRENT);
  if (m_nLastBars != nBars) {
    m_nLastBars = nBars;
    m_bNewBar = true;
  }
  return m_bNewBar;
}
