//+------------------------------------------------------------------+
//|                                             12movingAverages.mq5 | 
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright   "Noureddine Haouari"
#property link        "https://t.me/joinchat/Mj8ZLBO9x56DZgN2vIwq7w"
#property description "12 movingAverages by Noureddine Haouari "
#define VERSION "1.0.0"
#property version VERSION
//--- indicator settings
#property indicator_buffers 13
#property indicator_plots   0

input int max_period_number = 200;
input bool enhancedPowerCalculation = false;// 
input double historicalFactor= 0; 
input double instantaneousFactor= 1; 

input int MA1_Period = 10; // Moving Average period 01
input int MA2_Period = 30; // Moving Average period 02
input int MA3_Period = 30; // Moving Average period 03
input int MA4_Period = 40; // Moving Average period 04
input int MA5_Period = 50; // Moving Average period 05
input int MA6_Period = 60; // Moving Average period 06
input int MA7_Period = 70; // Moving Average period 07
input int MA8_Period = 80; // Moving Average period 08
input int MA9_Period = 90; // Moving Average period 09
input int MA10_Period = 100; // Moving Average period 10
input int MA11_Period = 150; // Moving Average period 11
input int MA12_Period = 200; // Moving Average period 12


// Moving Averages 
int MA1_handler = 0;
int MA2_handler = 0;
int MA3_handler = 0;
int MA4_handler = 0;
int MA5_handler = 0;
int MA6_handler = 0;
int MA7_handler = 0;
int MA8_handler = 0;
int MA9_handler = 0;
int MA10_handler = 0;
int MA11_handler = 0;
int MA12_handler = 0;

double MA1_buffer[];
double MA2_buffer[];
double MA3_buffer[];
double MA4_buffer[];
double MA5_buffer[];
double MA6_buffer[];
double MA7_buffer[];
double MA8_buffer[];
double MA9_buffer[];
double MA10_buffer[];
double MA11_buffer[];
double MA12_buffer[];






int correct[13]= {0,0,0,0,0,0,0,0,0,0,0,0,0};
int wrong[13]= {0,0,0,0,0,0,0,0,0,0,0,0,0};

int maxConsWrong[12]= {0,0,0,0,0,0,0,0,0,0,0,0};
int consWrongCompter[12]= {0,0,0,0,0,0,0,0,0,0,0,0};
int maxConsCorrect[12]= {0,0,0,0,0,0,0,0,0,0,0,0};
int consCorrectCompter[12]= {0,0,0,0,0,0,0,0,0,0,0,0};



string indicatorslist[12];

double power_buffer[];

//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
void OnInit()
  {
  
  indicatorslist[0]="MA "+MA1_Period;
  indicatorslist[1]="MA "+MA2_Period;
  indicatorslist[2]="MA "+MA3_Period;
  indicatorslist[3]="MA "+MA4_Period;
  indicatorslist[4]="MA "+MA5_Period;
  indicatorslist[5]="MA "+MA6_Period;
  indicatorslist[6]="MA "+MA7_Period;
  indicatorslist[7]="MA "+MA8_Period;
  indicatorslist[8]="MA "+MA9_Period;
  indicatorslist[9]="MA "+MA10_Period;
  indicatorslist[10]="MA "+MA11_Period;
  indicatorslist[11]="MA "+MA12_Period;          


   //--- indicator buffers mapping
   //TesterHideIndicators(true);
   SetIndexBuffer(0,MA1_buffer,INDICATOR_CALCULATIONS);
   SetIndexBuffer(1,MA2_buffer,INDICATOR_CALCULATIONS);
   SetIndexBuffer(2,MA3_buffer,INDICATOR_CALCULATIONS);
   SetIndexBuffer(3,MA4_buffer,INDICATOR_CALCULATIONS);
   SetIndexBuffer(4,MA5_buffer,INDICATOR_CALCULATIONS);
   SetIndexBuffer(5,MA6_buffer,INDICATOR_CALCULATIONS);
   SetIndexBuffer(6,MA7_buffer,INDICATOR_CALCULATIONS);
   SetIndexBuffer(7,MA8_buffer,INDICATOR_CALCULATIONS);
   SetIndexBuffer(8,MA9_buffer,INDICATOR_CALCULATIONS);
   SetIndexBuffer(9,MA10_buffer,INDICATOR_CALCULATIONS);
   SetIndexBuffer(10,MA11_buffer,INDICATOR_CALCULATIONS);
   SetIndexBuffer(11,MA12_buffer,INDICATOR_CALCULATIONS);
   SetIndexBuffer(12,power_buffer,INDICATOR_CALCULATIONS);
   
     
    createMenu();
    //MA
    MA1_handler = iMA(_Symbol, _Period, MA1_Period, 0, MODE_SMA, PRICE_CLOSE);
    MA2_handler= iMA(_Symbol, _Period, MA2_Period, 0, MODE_SMA, PRICE_CLOSE);
    MA3_handler = iMA(_Symbol, _Period, MA3_Period, 0, MODE_SMA, PRICE_CLOSE);
    MA4_handler = iMA(_Symbol, _Period, MA4_Period, 0, MODE_SMA, PRICE_CLOSE);
    MA5_handler= iMA(_Symbol, _Period, MA5_Period, 0, MODE_SMA, PRICE_CLOSE);
    MA6_handler = iMA(_Symbol, _Period, MA6_Period, 0, MODE_SMA, PRICE_CLOSE);
    MA7_handler = iMA(_Symbol, _Period, MA7_Period, 0, MODE_SMA, PRICE_CLOSE);
    MA8_handler= iMA(_Symbol, _Period, MA8_Period, 0, MODE_SMA, PRICE_CLOSE);
    MA9_handler = iMA(_Symbol, _Period, MA9_Period, 0, MODE_SMA, PRICE_CLOSE);
    MA10_handler = iMA(_Symbol, _Period, MA10_Period, 0, MODE_SMA, PRICE_CLOSE);
    MA11_handler= iMA(_Symbol, _Period, MA11_Period, 0, MODE_SMA, PRICE_CLOSE);
    MA12_handler = iMA(_Symbol, _Period, MA12_Period, 0, MODE_SMA, PRICE_CLOSE);
      
  }
  
  
  
//+------------------------------------------------------------------+
//| deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---

ObjectsDeleteAll(0);
  }
    
//+------------------------------------------------------------------+
//| Average True Range                                               |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
  
   int i,limit;
//--- check for bars count
   if(rates_total<max_period_number)
      return(0);// not enough bars for calculation   

//--- we can copy not all data
   int to_copy;
   if(prev_calculated>rates_total || prev_calculated<0) to_copy=rates_total;
   else
     {
      to_copy=rates_total-prev_calculated;
      if(prev_calculated>0) to_copy++;
     }
//---- get ma buffers
   if(IsStopped()) return(0); //Checking for stop flag
  
//--- first calculation or number of bars was changed
   if(prev_calculated<max_period_number)
      limit=100;
   else limit=prev_calculated-1;
//--- the main loop of calculations


   for(i=limit;i<rates_total-1 && !IsStopped();i++)
     {
     
     int results[12];
     calculate(results,open,high,low,close,i,to_copy);
    
     MA1_buffer[i]=results[0];
     MA2_buffer[i]=results[1];
     MA3_buffer[i]=results[2];
     MA4_buffer[i]=results[3];
     MA5_buffer[i]=results[4];
     MA6_buffer[i]=results[5];
     MA7_buffer[i]=results[6];
     MA8_buffer[i]=results[7];
     MA9_buffer[i]=results[8];
     MA10_buffer[i]=results[9];
     MA11_buffer[i]=results[10];
     MA12_buffer[i]=results[11];
     power_buffer[i]=getPower(results)*100;
     UpdatePowerStatus(open,close,results,i);
     for (int j = 0; j < 12; j++) { 
       resultsText(open,close,j, results[j], i);
      }
     }
//--- return value of prev_calculated for next call
   return(rates_total);
  }
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Get the signle of each indicator                                      |
//+------------------------------------------------------------------+
void calculate(int &results[], const double &open[],const double &high[], const double &low[],const double &close[], int index,int to_copy) {

  int start=0; 
  
  double ma1[];
  double ma2[];
  double ma3[];
  double ma4[];
  double ma5[];
  double ma6[];
  double ma7[];
  double ma8[];
  double ma9[];
  double ma10[];
  double ma11[];
  double ma12[];  
  
  if (CopyBuffer(MA1_handler, 0, start, 1, ma1) != 1 || CopyBuffer(MA2_handler, 0, start, 1, ma2) != 1 || CopyBuffer(MA3_handler, 0, start, 1, ma3) != 1 || CopyBuffer(MA4_handler, 0, start, 1, ma4) != 1 || CopyBuffer(MA5_handler, 0, start, 1, ma5) != 1 || CopyBuffer(MA6_handler, 0, start, 1, ma6) != 1 || CopyBuffer(MA7_handler, 0, start, 1, ma7) != 1 || CopyBuffer(MA8_handler, 0, start, 1, ma8) != 1 || CopyBuffer(MA9_handler, 0, start, 1, ma9) != 1 || CopyBuffer(MA10_handler, 0, start, 1, ma10) != 1 || CopyBuffer(MA11_handler, 0, start, 1, ma11) != 1 || CopyBuffer(MA12_handler, 0, start, 1, ma12) != 1) {
    Print("CopyBuffer from iMA failed, no data");
    return;
  }
   checkMovingAverage(results[0],ma1[0], close[index]);
   checkMovingAverage(results[1],ma2[0], close[index]);
   checkMovingAverage(results[2],ma3[0], close[index]);
   checkMovingAverage(results[3],ma4[0], close[index]);
   checkMovingAverage(results[4],ma5[0], close[index]);
   checkMovingAverage(results[5],ma6[0], close[index]);
   checkMovingAverage(results[6],ma7[0], close[index]);
   checkMovingAverage(results[7],ma8[0], close[index]);
   checkMovingAverage(results[8],ma9[0], close[index]);
   checkMovingAverage(results[9],ma10[0], close[index]);
   checkMovingAverage(results[10],ma11[0], close[index]);
   checkMovingAverage(results[11],ma12[0], close[index]);
}

void checkMovingAverage(int &result,double MA,double close){
  result = 0; 
  if ( close  > MA){result = 1;}
  else if ( close  < MA){result = -1;}

}



//+------------------------------------------------------------------+
//| Calculate power                                    |
//+------------------------------------------------------------------+


double getPower(int &results[]){
  if(enhancedPowerCalculation){
  return getPower2(results);
  } else {
  return getPower1(results);
  }
}



double getPower1(const int &results[]){

   double power= 0;

   for(int i=0;i<12;i++){
    power+=0.083*results[i];
   }
    
   return power;
}


double getPower2(int & results[]){
   double sumPos=0;
   double sumNeg=0;
   
   for(int i=0;i<12;i++){
     double instantaneousWeight= (double)consCorrectCompter[i] / (double)(MathMax(consCorrectCompter[i]+consWrongCompter[i] ,1));
     double historyWeight = (double)correct[i] / (double)(MathMax(correct[i]+wrong[i] ,1));
     double weight=historicalFactor*instantaneousWeight+instantaneousFactor*historyWeight;
     //double weight=1;
      if(results[i]>0){
        sumPos+=weight;    
      } else if (results[i]<0) {
        sumNeg+=weight;    
      }
   }
   
    double result=0; 

    if(sumPos>sumNeg &&  sumPos!= 0) {
      result = 1.0 - sumNeg / sumPos;
      }
      
    if(sumPos<sumNeg &&  sumNeg!= 0){ 
      result = -1*(1.0-sumPos / sumNeg);
      }
      
      Print("Power == ",result,"/ sumNeg = ",sumNeg, "sumPos = ",sumPos," weight = ");
   return result;
  
}
//+------------------------------------------------------------------+
//| Get results i                                    |
//+------------------------------------------------------------------+

void getResults(int &results[],int i) {
    results[0]= MA1_buffer[i];
    results[1]= MA2_buffer[i];
    results[2]= MA3_buffer[i];
    results[3]= MA4_buffer[i];
    results[4]= MA5_buffer[i];
    results[5]= MA6_buffer[i];
    results[6]= MA7_buffer[i];
    results[7]= MA8_buffer[i];
    results[8]= MA9_buffer[i];
    results[9]= MA10_buffer[i];
    results[10]= MA11_buffer[i];
    results[11]= MA12_buffer[i];
}


//+------------------------------------------------------------------+
//| Create the graphic menu                                   |
//+------------------------------------------------------------------+
void createMenu() {
    // Header 
   string headerBg="Header BG";
    ObjectCreate(ChartID(), headerBg, OBJ_RECTANGLE_LABEL, 0, 0, 0);
    ObjectSetInteger(ChartID(), headerBg, OBJPROP_XDISTANCE, 0);
    ObjectSetInteger(ChartID(), headerBg, OBJPROP_YDISTANCE, 0);
    ObjectSetInteger(ChartID(), headerBg, OBJPROP_XSIZE, 430);
    ObjectSetInteger(ChartID(), headerBg, OBJPROP_YSIZE, 40);
    ObjectSetInteger(ChartID(), headerBg, OBJPROP_BGCOLOR, clrWhite);
    ObjectSetInteger(ChartID(), headerBg, OBJPROP_BORDER_COLOR, clrBlack);
    ObjectSetInteger(ChartID(), headerBg, OBJPROP_BORDER_TYPE, BORDER_FLAT);
    ObjectSetInteger(ChartID(), headerBg, OBJPROP_WIDTH, 0);
    ObjectSetInteger(ChartID(), headerBg, OBJPROP_BACK, false);
    ObjectSetInteger(ChartID(), headerBg, OBJPROP_SELECTABLE, false);
    ObjectSetInteger(ChartID(), headerBg, OBJPROP_HIDDEN, true);
    string header = "12MovingAvg";
    ObjectCreate(0, header, OBJ_LABEL, 0, 0, 0, 0, 0, 0, 0);
    ObjectSetInteger(0, header, OBJPROP_XDISTANCE, 10);

    ObjectSetInteger(0, header, OBJPROP_YDISTANCE, 5);
    ObjectSetInteger(0, header, OBJPROP_CORNER, CORNER_LEFT_UPPER);
    ObjectSetInteger(0, header, OBJPROP_COLOR, clrBlack);
    ObjectSetInteger(0, header, OBJPROP_BORDER_COLOR, clrAqua);
    ObjectSetString(0, header, OBJPROP_FONT, "Arial");
    ObjectSetInteger(0, header, OBJPROP_FONTSIZE, 12);
    ObjectSetString(0, header, OBJPROP_TEXT, header);


 string powerBg="PowerPg";
    ObjectCreate(ChartID(), powerBg, OBJ_RECTANGLE_LABEL, 0, 0, 0);
    ObjectSetInteger(ChartID(), powerBg, OBJPROP_XDISTANCE, 0);
    ObjectSetInteger(ChartID(), powerBg, OBJPROP_YDISTANCE, 40);
    ObjectSetInteger(ChartID(), powerBg, OBJPROP_XSIZE, 430);
    ObjectSetInteger(ChartID(), powerBg, OBJPROP_YSIZE, 40);
    ObjectSetInteger(ChartID(), powerBg, OBJPROP_BGCOLOR, clrWhite);
    ObjectSetInteger(ChartID(), powerBg, OBJPROP_BORDER_COLOR, clrBlack);
    ObjectSetInteger(ChartID(), powerBg, OBJPROP_BORDER_TYPE, BORDER_FLAT);
    ObjectSetInteger(ChartID(), powerBg, OBJPROP_WIDTH, 0);
    ObjectSetInteger(ChartID(), powerBg, OBJPROP_BACK, false);
    ObjectSetInteger(ChartID(), powerBg, OBJPROP_SELECTABLE, false);
    ObjectSetInteger(ChartID(), powerBg, OBJPROP_HIDDEN, true);
    string power = "Power";
    ObjectCreate(0, power, OBJ_LABEL, 0, 0, 0, 0, 0, 0, 0);
    ObjectSetInteger(0, power, OBJPROP_XDISTANCE, 10);
    ObjectSetInteger(0, power, OBJPROP_YDISTANCE, 45);
    
    ObjectSetInteger(0, power, OBJPROP_CORNER, CORNER_LEFT_UPPER);
    ObjectSetInteger(0, power, OBJPROP_COLOR, clrBlack);
    ObjectSetInteger(0, power, OBJPROP_BORDER_COLOR, clrGreen);
    ObjectSetString(0, power, OBJPROP_FONT, "Arial");
    ObjectSetInteger(0, power, OBJPROP_FONTSIZE, 14);
    ObjectSetString(0, power, OBJPROP_TEXT, power);


    // *** Background
    
  int y = 60;
  for (int i = 0; i < 12; i++) {
    y += 25;
    string labelName = indicatorslist[i] + "Label";
    string value = indicatorslist[i];
    string objectBackgroundName = indicatorslist[i] + "bg";
    string objectBackgroundLabelName = labelName + "Label-bg";

    ObjectCreate(ChartID(), objectBackgroundName, OBJ_RECTANGLE_LABEL, 0, 0, 0);
    ObjectSetInteger(ChartID(), objectBackgroundName, OBJPROP_XDISTANCE, 220);
    ObjectSetInteger(ChartID(), objectBackgroundName, OBJPROP_YDISTANCE, y);
    ObjectSetInteger(ChartID(), objectBackgroundName, OBJPROP_XSIZE, 200);
    ObjectSetInteger(ChartID(), objectBackgroundName, OBJPROP_YSIZE, 25);
    ObjectSetInteger(ChartID(), objectBackgroundName, OBJPROP_BGCOLOR, clrWhite);
    ObjectSetInteger(ChartID(), objectBackgroundName, OBJPROP_BORDER_COLOR, clrBlack);
    ObjectSetInteger(ChartID(), objectBackgroundName, OBJPROP_BORDER_TYPE, BORDER_FLAT);
    ObjectSetInteger(ChartID(), objectBackgroundName, OBJPROP_WIDTH, 0);
    ObjectSetInteger(ChartID(), objectBackgroundName, OBJPROP_BACK, false);
    ObjectSetInteger(ChartID(), objectBackgroundName, OBJPROP_SELECTABLE, false);
    ObjectSetInteger(ChartID(), objectBackgroundName, OBJPROP_HIDDEN, true);

    ObjectCreate(ChartID(), objectBackgroundLabelName, OBJ_RECTANGLE_LABEL, 0, 0, 0);
    ObjectSetInteger(ChartID(), objectBackgroundLabelName, OBJPROP_XDISTANCE, 10);
    ObjectSetInteger(ChartID(), objectBackgroundLabelName, OBJPROP_YDISTANCE, y);
    ObjectSetInteger(ChartID(), objectBackgroundLabelName, OBJPROP_XSIZE, 200);
    ObjectSetInteger(ChartID(), objectBackgroundLabelName, OBJPROP_YSIZE, 25);
    ObjectSetInteger(ChartID(), objectBackgroundLabelName, OBJPROP_BGCOLOR, clrWhite);
    ObjectSetInteger(ChartID(), objectBackgroundLabelName, OBJPROP_BORDER_COLOR, clrBlack);
    ObjectSetInteger(ChartID(), objectBackgroundLabelName, OBJPROP_BORDER_TYPE, BORDER_FLAT);
    ObjectSetInteger(ChartID(), objectBackgroundLabelName, OBJPROP_WIDTH, 0);
    ObjectSetInteger(ChartID(), objectBackgroundLabelName, OBJPROP_BACK, false);
    ObjectSetInteger(ChartID(), objectBackgroundLabelName, OBJPROP_SELECTABLE, false);
    ObjectSetInteger(ChartID(), objectBackgroundLabelName, OBJPROP_HIDDEN, true);

    ObjectCreate(0, labelName, OBJ_LABEL, 0, 0, 0, 0, 0, 0, 0);
    ObjectSetInteger(0, labelName, OBJPROP_XDISTANCE, 20);
    ObjectSetInteger(0, labelName, OBJPROP_YDISTANCE, y);
    ObjectSetInteger(0, labelName, OBJPROP_CORNER, CORNER_LEFT_UPPER);
    ObjectSetInteger(0, labelName, OBJPROP_COLOR, clrBlack);
    ObjectSetInteger(0, labelName, OBJPROP_BORDER_COLOR, clrAqua);
    ObjectSetString(0, labelName, OBJPROP_TEXT, indicatorslist[i]);
    ObjectSetString(0, labelName, OBJPROP_FONT, "Arial");
    ObjectSetInteger(0, labelName, OBJPROP_FONTSIZE, 12);

    ObjectCreate(0, value, OBJ_LABEL, 0, 0, 0, 0, 0, 0, 0);
    ObjectSetInteger(0, value, OBJPROP_XDISTANCE, 230);
    ObjectSetInteger(0, value, OBJPROP_YDISTANCE, y);
    ObjectSetInteger(0, value, OBJPROP_CORNER, CORNER_LEFT_UPPER);
    ObjectSetInteger(0, value, OBJPROP_COLOR, clrBlack);
    ObjectSetInteger(0, value, OBJPROP_BORDER_COLOR, clrAqua);
    ObjectSetString(0, value, OBJPROP_FONT, "Arial");
    ObjectSetInteger(0, value, OBJPROP_FONTSIZE, 12);
    ObjectSetString(0, value, OBJPROP_TEXT, "No Trade");

  }
}

//+------------------------------------------------------------------+
//| Get signal text (Buy sell no trade)                                       |
//+------------------------------------------------------------------+
string resultsText(const double &open[],const double &close[],int indicatorIndex, int value, int index) {
   
    
  int preresults[12];
  getResults(preresults,index-1);
  
  if((preresults[indicatorIndex]==-1&&close[index-1]<open[index-1])||(preresults[indicatorIndex]==1&&close[index-1]>open[index-1])){
   correct[indicatorIndex]++;
   consCorrectCompter[indicatorIndex]++;
   consWrongCompter[indicatorIndex]=0;
  } else if((preresults[indicatorIndex]==-1&&close[index-1]>open[index-1])||(preresults[indicatorIndex]==1&&close[index-1]<open[index-1])){
   wrong[indicatorIndex]++;
   consWrongCompter[indicatorIndex]++;
   consCorrectCompter[indicatorIndex]=0;
  }
  
  
  bool changed= true;
  if(preresults[indicatorIndex]==value){changed=false;}
  string signal ="";
  if (value == 1) {
    signal = "Buy";
    ObjectSetInteger(ChartID(), indicatorslist[indicatorIndex] + "bg", OBJPROP_BGCOLOR, clrGreen);
  } else if (value == -1) {
    signal = "Sell";
    ObjectSetInteger(ChartID(), indicatorslist[indicatorIndex] + "bg", OBJPROP_BGCOLOR, clrOrange);
  } else {
    signal= "Wait";
    ObjectSetInteger(ChartID(), indicatorslist[indicatorIndex] + "bg", OBJPROP_BGCOLOR, clrWheat);
  }

  if (changed) {
    signal = signal + "*";
  }
  
  double successRate= 100*(double)correct[indicatorIndex]/(double)(MathMax(correct[indicatorIndex]+wrong[indicatorIndex],1));

  ObjectSetString(0, indicatorslist[indicatorIndex], OBJPROP_TEXT, signal+" | SuccRate="+ MathRound(successRate)+"%");

  return indicatorslist[indicatorIndex] + " : " + signal +" ("+ MathRound(successRate)+"%)\n";

}


void UpdatePowerStatus(const double &open[],const double &close[],int &results[],int index){

    double power=power_buffer[index];
    
    double prePower= power_buffer[index-1]; 
  
  if((prePower<0&&close[index-1]<open[index-1])||(prePower>0&&close[index-1]>open[index-1])){
   correct[12]++;
  } else if((prePower<0&&close[index-1]>open[index-1])||(prePower>0&&close[index-1]<open[index-1])){
   wrong[12]++;
  }
  
 
    string globalSignal;
   // double successRate =100*(double)powerCorrect/MathMax((double)(powerCorrect+powerWrong),1) ; 
    if(power > 0)  {
       globalSignal= "Buy";
       ObjectSetInteger(ChartID(), "PowerPg", OBJPROP_BGCOLOR, clrGreen);
    } 
    
    if(power < 0)  {
       globalSignal= "Sell";
       ObjectSetInteger(ChartID(), "PowerPg", OBJPROP_BGCOLOR, clrOrange);
    }
    
    if(power == 0)  {
       globalSignal= "No Trade ";
       ObjectSetInteger(ChartID(), "PowerPg", OBJPROP_BGCOLOR, clrWheat);
    }
    
    datetime dt = TimeCurrent();
    MqlDateTime tm; 
      TimeToStruct(dt,tm);
    string date=(string)tm.day+"-"+(string)tm.mon+"-" +(string)tm.year;
 
    ObjectSetString(0, "12MovingAvg", OBJPROP_TEXT, "12MovingAvg-V"+VERSION+" | "+Symbol()+" ("+ date+") ");
    double successRate= 100*(double)correct[12]/(double)(MathMax(correct[12]+wrong[12],1)); 
    
   // Print("Power succrate =  ",successRate," | number of mesures = ",correct[12]+wrong[12]);
    ObjectSetString(0, "Power", OBJPROP_TEXT, globalSignal+ " | Power = "+(string) MathRound(power)+"%| SuccRate = "+ MathRound(successRate)+"%");
}
