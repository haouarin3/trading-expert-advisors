//+------------------------------------------------------------------+
//|                                                 12indicators.mq5 | 
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright   "Noureddine Haouari"
#property link        "https://t.me/joinchat/Mj8ZLBO9x56DZgN2vIwq7w"
#property description "12 indicators by Noureddine Haouari "
#define VERSION "1.0.7"
#property version VERSION
//--- indicator settings
#property indicator_buffers 13
#property indicator_plots   0

input int max_period_number = 100;
input bool enhancedPowerCalculation = false;// 
input double historicalFactor= 0; 
input double instantaneousFactor= 1; 

input int MV1_Period = 20; // Moving Average period 01
input int MV2_Period = 50; // Moving Average period 02
input int MV3_Period = 100; // Moving Average period 03


input int MACD1FAST_Period = 20;
input int MACD2FAST_Period = 20;
input int MACD3FAST_Period = 50;

input int MACD1SlOW_Period = 50;
input int MACD2SlOW_Period = 100;
input int MACD3SlOW_Period = 100;


input int MACD1_Signal = 9;
input int MACD2_Signal = 9;
input int MACD3_Signal = 9;

input int CCI1_Period = 40;
input int CCI2_Period = 60;

input int BB_Period = 20;

input double SAR_acceleration_factor = 0.02;
input double SAR_maximum = 0.2; // maximum value of step 

input int ADX_Period = 7;

input int AHC_Low_Period = 8;
input int AHC_High_Period = 10;


// Moving Averages 
int mv20_handler = 0;
int mv50_handler = 0;
int mv100_handler = 0;

double mv20_buffer[];
double mv50_buffer[];
double mv100_buffer[];
// MACD 
int macd20_50_handler = 0;
int macd20_100_handler = 0;
int macd50_100_handler = 0;

double macd20_50_buffer[];
double macd20_100_buffer[];
double macd50_100_buffer[];

// CCI 
int cci40_handler = 0;
int cci60_handler = 0;

double cci40_buffer[];
double cci60_buffer[];

//Parabolic 
int sar_handler = 0;
double sar_buffer[];

//Bollinger Bands 
int iBands__handler = 0;
double iBands_buffer[];

//ADX 
int adx_handler = 0;
double adx_buffer[];

//Average Hilo Channel 
int ahc_mv8_handler = 0;
int ahc_mv10_handler = 0;

double ahc_buffer[];
int correct[13]= {0,0,0,0,0,0,0,0,0,0,0,0,0};
int wrong[13]= {0,0,0,0,0,0,0,0,0,0,0,0,0};

int maxConsWrong[12]= {0,0,0,0,0,0,0,0,0,0,0,0};
int consWrongCompter[12]= {0,0,0,0,0,0,0,0,0,0,0,0};
int maxConsCorrect[12]= {0,0,0,0,0,0,0,0,0,0,0,0};
int consCorrectCompter[12]= {0,0,0,0,0,0,0,0,0,0,0,0};



string indicatorslist[12] = {
  "IND1_ADX",
  "IND2_AHC",
  "IND3_MA20",
  "IND4_MACD_20_50",
  "IND5_BB",
  "IND6_CCI40",
  "IND7_MA50",
  "IND8_MACD_20_100",
  "IND9_SAR",
  "IND10_CCI60",
  "IND11_MA100",
  "IND12_MACD_50_100"
};

double power_buffer[];

//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
void OnInit()
  {
   //--- indicator buffers mapping
   TesterHideIndicators(true);
   SetIndexBuffer(0,adx_buffer,INDICATOR_CALCULATIONS);
   SetIndexBuffer(1,ahc_buffer,INDICATOR_CALCULATIONS);
   SetIndexBuffer(2,mv20_buffer,INDICATOR_CALCULATIONS);
   SetIndexBuffer(3,macd20_50_buffer,INDICATOR_CALCULATIONS);
   SetIndexBuffer(4,iBands_buffer,INDICATOR_CALCULATIONS);
   SetIndexBuffer(5,cci40_buffer,INDICATOR_CALCULATIONS);
   SetIndexBuffer(6,mv50_buffer,INDICATOR_CALCULATIONS);
   SetIndexBuffer(7,macd20_100_buffer,INDICATOR_CALCULATIONS);
   SetIndexBuffer(8,sar_buffer,INDICATOR_CALCULATIONS);
   SetIndexBuffer(9,cci60_buffer,INDICATOR_CALCULATIONS);
   SetIndexBuffer(10,mv100_buffer,INDICATOR_CALCULATIONS);
   SetIndexBuffer(11,macd50_100_buffer,INDICATOR_CALCULATIONS);
   SetIndexBuffer(12,power_buffer,INDICATOR_CALCULATIONS);
   
     
    createMenu();
    //MA
    mv20_handler = iMA(_Symbol, _Period, MV1_Period, 0, MODE_SMA, PRICE_CLOSE);
    mv50_handler = iMA(_Symbol, _Period, MV2_Period, 0, MODE_SMA, PRICE_CLOSE);
    mv100_handler = iMA(_Symbol, _Period, MV3_Period, 0, MODE_SMA, PRICE_CLOSE);


    // MACD
    macd20_50_handler = iMACD(_Symbol, _Period, MACD1FAST_Period,MACD1SlOW_Period, MACD1_Signal, PRICE_CLOSE);
    macd20_100_handler = iMACD(_Symbol, _Period, MACD2FAST_Period, MACD2SlOW_Period, MACD2_Signal, PRICE_CLOSE);
    macd50_100_handler = iMACD(_Symbol, _Period, MACD3FAST_Period, MACD3SlOW_Period, MACD3_Signal, PRICE_CLOSE);


    //CCI
    cci40_handler = iCCI(_Symbol, _Period, CCI1_Period, PRICE_TYPICAL);
    cci60_handler = iCCI(_Symbol, _Period, CCI2_Period, PRICE_TYPICAL);

    //BB
    iBands__handler = iBands(_Symbol, _Period, BB_Period, 0, 2, PRICE_CLOSE);

    //SAR
    sar_handler = iSAR(_Symbol, _Period, SAR_acceleration_factor, SAR_maximum);

    //ADX iClose

    adx_handler = iADX(_Symbol, _Period, ADX_Period);

    //Average Hilo Channel 
    ahc_mv8_handler = iMA(_Symbol, _Period, AHC_Low_Period, 0, MODE_SMA, PRICE_LOW);
    ahc_mv10_handler = iMA(_Symbol, _Period, AHC_High_Period, 0, MODE_SMA, PRICE_HIGH);
    //---
    
    
  }
  
  
//+------------------------------------------------------------------+
//| deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
   ObjectsDeleteAll(0);
  }
      
//+------------------------------------------------------------------+
//| Average True Range                                               |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
  
   int i,limit;
//--- check for bars count
   if(rates_total<max_period_number)
      return(0);// not enough bars for calculation   

//--- we can copy not all data
   int to_copy;
   if(prev_calculated>rates_total || prev_calculated<0) to_copy=rates_total;
   else
     {
      to_copy=rates_total-prev_calculated;
      if(prev_calculated>0) to_copy++;
     }
//---- get ma buffers
   if(IsStopped()) return(0); //Checking for stop flag
  
//--- first calculation or number of bars was changed
   if(prev_calculated<max_period_number)
      limit=100;
   else limit=prev_calculated-1;
//--- the main loop of calculations


   for(i=limit;i<rates_total-1 && !IsStopped();i++)
     {
     
     int results[12];
     calculate(results,open,high,low,close,i,to_copy);
    
     adx_buffer[i]=results[0];
     ahc_buffer[i]=results[1];
     mv20_buffer[i]=results[2];
     macd20_50_buffer[i]=results[3];
     iBands_buffer[i]=results[4];
     cci40_buffer[i]=results[5];
     mv50_buffer[i]=results[6];
     macd20_100_buffer[i]=results[7];
     sar_buffer[i]=results[8];
     cci60_buffer[i]=results[9];
     mv100_buffer[i]=results[10];
     macd50_100_buffer[i]=results[11];
     power_buffer[i]=getPower(results)*100;
     UpdatePowerStatus(open,close,results,i);
     for (int j = 0; j < 12; j++) { 
       resultsText(open,close,j, results[j], i);
      }
     }
//--- return value of prev_calculated for next call
   return(rates_total);
  }
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Get the signle of each indicator                                      |
//+------------------------------------------------------------------+
void calculate(int &results[], const double &open[],const double &high[], const double &low[],const double &close[], int indexP,int to_copy) {

  int start=0;
  int index=indexP;
  double ma20[];
  double ma50[];
  double ma100[];
  if (CopyBuffer(mv20_handler, 0, start, 1, ma20) != 1 || CopyBuffer(mv50_handler, 0, start, 1, ma50) != 1 || CopyBuffer(mv100_handler, 0, start, 1, ma100) != 1) {
    Print("CopyBuffer from iMA failed, no data");
    return;
  }

  results[2] =0;
  results[6] =0;
  results[10]=0;
  
  if ( close[index]  > ma20[0]){results[2] = 1;}
  if ( close[index]  < ma20[0]){results[2] = -1;}
  
  if ( close[index]  > ma50[0]){results[6] = 1;}
  if ( close[index]  < ma50[0]){results[6] = -1;}
  
  if ( close[index]  > ma100[0]){results[10] = 1;}
  if ( close[index]  < ma100[0]){results[10] = -1;}
  
  

   

  //MACD 
  double macd20_50[];
  double macd20_100[];
  double macd50_100[];

  if (CopyBuffer(macd20_50_handler, 1, start, 1, macd20_50) != 1 || CopyBuffer(macd20_100_handler, 1, start, 1, macd20_100) != 1 || CopyBuffer(macd50_100_handler, 1, start, 1, macd50_100) != 1) {
    Print("CopyBuffer from MACD failed, no data");
    return;
  }
  
  results[3]=0;
  results[7]=0;
  results[11]=0;
  if ( macd20_50[0] > 0){results[3] = 1;}
  if ( macd20_50[0] < 0){results[3] = -1;}
  
  if ( macd20_100[0] > 0){results[7] = 1;}
  if (  macd20_100[0] < 0){results[7] = -1;}
  
  if ( macd50_100[0] > 0){results[11] = 1;}
  if ( macd50_100[0] < 0){results[11] = -1;}
  

  double cci40[];
  double cci60[];

  if (CopyBuffer(cci40_handler, 0, start, 1, cci40) != 1 || CopyBuffer(cci60_handler, 0, start, 1, cci60) != 1) {
    Print("CopyBuffer from CCI failed, no data");
    return;
  }
  
   results[5]=0;
   if (cci40[0] > 100){results[5] = 1;}
   if (cci40[0] < -100){results[5] = -1;}
   
   results[9]=0;
   if (cci60[0] > 100){results[9] = 1;}
   if (cci60[0] < -100){results[9] = -1;} 



  //BBAND
  double baseLine[];
  double lowerBand[];
  double upperBand[];
  // 0 - BASE_LINE, 1 - UPPER_BAND, 2 - LOWER_BAND
  if (CopyBuffer(iBands__handler, 0, start, 1, baseLine) != 1 || CopyBuffer(iBands__handler, 1, start, 1, upperBand) != 1 || CopyBuffer(iBands__handler, 2, start, 1, lowerBand) != 1) {
    Print("CopyBuffer from BBAND failed, no data");
    return;
  }
  
  results[4]=0;
  if (close[index] > upperBand[0]){ results[4] = 1;}
  if (close[index] < lowerBand[0]){ results[4] = -1;}


 //SAR
  double sar[1];
  if (CopyBuffer(sar_handler, 0, start, 1, sar) != 1) {
    Print("CopyBuffer from sar failed, no data");
    return;
  }
  results[8]=0;
  if (sar[0] < close[index]){ results[8] = 1;}
  if (sar[0] > close[index]){ results[8] = -1;}
 
  
  //The buffer numbers are the following: 0 - MAIN_LINE, 1 - PLUSDI_LINE, 2 - MINUSDI_LINE.
  //ADX 
  double adx[];
  double pdi[];
  double mdi[];

  if (CopyBuffer(adx_handler, 0, start, 1, adx) != 1 || CopyBuffer(adx_handler, 1, start, 1, pdi) != 1 || CopyBuffer(adx_handler, 2, start, 1, mdi) != 1) {
    Print("CopyBuffer from ahc failed, no data");
    return;
  }

   results[0] =0;
   if (pdi[0] > mdi[0] ){ results[0] = 1;}
   if (pdi[0] < mdi[0] ){ results[0] = -1;}
   
  //AHC
  double ahc_mv8[];
  double ahc_mv10[];
  if (CopyBuffer(ahc_mv8_handler, 0, start, 2, ahc_mv8) != 2 || CopyBuffer(ahc_mv10_handler, 0, start, 2, ahc_mv10) != 2) {
    Print("CopyBuffer from ahc failed, no data");
    return;
  }

  ArraySetAsSeries(ahc_mv8, true);
  ArraySetAsSeries(ahc_mv10, true);
  
   results[1]=0;  
   if (close[index] >  ahc_mv10[0]){  results[1] =1;}
   if (close[index] < ahc_mv8[0]){ results[1] =-1;}

}
//+------------------------------------------------------------------+
//| Calculate power                                    |
//+------------------------------------------------------------------+


double getPower(int &results[]){
  if(enhancedPowerCalculation){
  return getPower2(results);
  } else {
  return getPower1(results);
  }
}



double getPower1(const int &results[]){

   double power= 0;

   for(int i=0;i<12;i++){
    power+=0.083*results[i];
   }
    
   return power;
}


double getPower2(int & results[]){
   double sumPos=0;
   double sumNeg=0;
   
   for(int i=0;i<12;i++){
     double instantaneousWeight= (double)consCorrectCompter[i] / (double)(MathMax(consCorrectCompter[i]+consWrongCompter[i] ,1));
     double historyWeight = (double)correct[i] / (double)(MathMax(correct[i]+wrong[i] ,1));
     double weight=historicalFactor*instantaneousWeight+instantaneousFactor*historyWeight;
     //double weight=1;
      if(results[i]>0){
        sumPos+=weight;    
      } else if (results[i]<0) {
        sumNeg+=weight;    
      }
   }
   
    double result=0; 

    if(sumPos>sumNeg &&  sumPos!= 0) {
      result = 1.0 - sumNeg / sumPos;
      }
      
    if(sumPos<sumNeg &&  sumNeg!= 0){ 
      result = -1*(1.0-sumPos / sumNeg);
      }
      
      Print("Power == ",result,"/ sumNeg = ",sumNeg, "sumPos = ",sumPos," weight = ");
   return result;
  
}
//+------------------------------------------------------------------+
//| Get results i                                    |
//+------------------------------------------------------------------+

void getResults(int &results[],int i) {
    results[0]= adx_buffer[i];
    results[1]= ahc_buffer[i];
    results[2]= mv20_buffer[i];
    results[3]= macd20_50_buffer[i];
    results[4]= iBands_buffer[i];
    results[5]=  cci40_buffer[i];
    results[6]=  mv50_buffer[i];
    results[7]= macd20_100_buffer[i];
    results[8]= sar_buffer[i];
    results[9]=  cci60_buffer[i];
    results[10]=  mv100_buffer[i];
    results[11]=  macd50_100_buffer[i];
}


//+------------------------------------------------------------------+
//| Create the graphic menu                                   |
//+------------------------------------------------------------------+
void createMenu() {
    // Header 
   string headerBg="Header BG";
    ObjectCreate(ChartID(), headerBg, OBJ_RECTANGLE_LABEL, 0, 0, 0);
    ObjectSetInteger(ChartID(), headerBg, OBJPROP_XDISTANCE, 0);
    ObjectSetInteger(ChartID(), headerBg, OBJPROP_YDISTANCE, 0);
    ObjectSetInteger(ChartID(), headerBg, OBJPROP_XSIZE, 430);
    ObjectSetInteger(ChartID(), headerBg, OBJPROP_YSIZE, 40);
    ObjectSetInteger(ChartID(), headerBg, OBJPROP_BGCOLOR, clrWhite);
    ObjectSetInteger(ChartID(), headerBg, OBJPROP_BORDER_COLOR, clrBlack);
    ObjectSetInteger(ChartID(), headerBg, OBJPROP_BORDER_TYPE, BORDER_FLAT);
    ObjectSetInteger(ChartID(), headerBg, OBJPROP_WIDTH, 0);
    ObjectSetInteger(ChartID(), headerBg, OBJPROP_BACK, false);
    ObjectSetInteger(ChartID(), headerBg, OBJPROP_SELECTABLE, false);
    ObjectSetInteger(ChartID(), headerBg, OBJPROP_HIDDEN, true);
    string header = "12 Indicators";
    ObjectCreate(0, header, OBJ_LABEL, 0, 0, 0, 0, 0, 0, 0);
    ObjectSetInteger(0, header, OBJPROP_XDISTANCE, 10);

    ObjectSetInteger(0, header, OBJPROP_YDISTANCE, 5);
    ObjectSetInteger(0, header, OBJPROP_CORNER, CORNER_LEFT_UPPER);
    ObjectSetInteger(0, header, OBJPROP_COLOR, clrBlack);
    ObjectSetInteger(0, header, OBJPROP_BORDER_COLOR, clrAqua);
    ObjectSetString(0, header, OBJPROP_FONT, "Arial");
    ObjectSetInteger(0, header, OBJPROP_FONTSIZE, 12);
    ObjectSetString(0, header, OBJPROP_TEXT, header);


 string powerBg="PowerPg";
    ObjectCreate(ChartID(), powerBg, OBJ_RECTANGLE_LABEL, 0, 0, 0);
    ObjectSetInteger(ChartID(), powerBg, OBJPROP_XDISTANCE, 0);
    ObjectSetInteger(ChartID(), powerBg, OBJPROP_YDISTANCE, 40);
    ObjectSetInteger(ChartID(), powerBg, OBJPROP_XSIZE, 430);
    ObjectSetInteger(ChartID(), powerBg, OBJPROP_YSIZE, 40);
    ObjectSetInteger(ChartID(), powerBg, OBJPROP_BGCOLOR, clrWhite);
    ObjectSetInteger(ChartID(), powerBg, OBJPROP_BORDER_COLOR, clrBlack);
    ObjectSetInteger(ChartID(), powerBg, OBJPROP_BORDER_TYPE, BORDER_FLAT);
    ObjectSetInteger(ChartID(), powerBg, OBJPROP_WIDTH, 0);
    ObjectSetInteger(ChartID(), powerBg, OBJPROP_BACK, false);
    ObjectSetInteger(ChartID(), powerBg, OBJPROP_SELECTABLE, false);
    ObjectSetInteger(ChartID(), powerBg, OBJPROP_HIDDEN, true);
    string power = "Power";
    ObjectCreate(0, power, OBJ_LABEL, 0, 0, 0, 0, 0, 0, 0);
    ObjectSetInteger(0, power, OBJPROP_XDISTANCE, 10);
    ObjectSetInteger(0, power, OBJPROP_YDISTANCE, 45);
    
    ObjectSetInteger(0, power, OBJPROP_CORNER, CORNER_LEFT_UPPER);
    ObjectSetInteger(0, power, OBJPROP_COLOR, clrBlack);
    ObjectSetInteger(0, power, OBJPROP_BORDER_COLOR, clrGreen);
    ObjectSetString(0, power, OBJPROP_FONT, "Arial");
    ObjectSetInteger(0, power, OBJPROP_FONTSIZE, 14);
    ObjectSetString(0, power, OBJPROP_TEXT, power);


    // *** Background
    
  int y = 60;
  for (int i = 0; i < 12; i++) {
    y += 25;
    string labelName = indicatorslist[i] + "Label";
    string value = indicatorslist[i];
    string objectBackgroundName = indicatorslist[i] + "bg";
    string objectBackgroundLabelName = labelName + "Label-bg";

    ObjectCreate(ChartID(), objectBackgroundName, OBJ_RECTANGLE_LABEL, 0, 0, 0);
    ObjectSetInteger(ChartID(), objectBackgroundName, OBJPROP_XDISTANCE, 220);
    ObjectSetInteger(ChartID(), objectBackgroundName, OBJPROP_YDISTANCE, y);
    ObjectSetInteger(ChartID(), objectBackgroundName, OBJPROP_XSIZE, 200);
    ObjectSetInteger(ChartID(), objectBackgroundName, OBJPROP_YSIZE, 25);
    ObjectSetInteger(ChartID(), objectBackgroundName, OBJPROP_BGCOLOR, clrWhite);
    ObjectSetInteger(ChartID(), objectBackgroundName, OBJPROP_BORDER_COLOR, clrBlack);
    ObjectSetInteger(ChartID(), objectBackgroundName, OBJPROP_BORDER_TYPE, BORDER_FLAT);
    ObjectSetInteger(ChartID(), objectBackgroundName, OBJPROP_WIDTH, 0);
    ObjectSetInteger(ChartID(), objectBackgroundName, OBJPROP_BACK, false);
    ObjectSetInteger(ChartID(), objectBackgroundName, OBJPROP_SELECTABLE, false);
    ObjectSetInteger(ChartID(), objectBackgroundName, OBJPROP_HIDDEN, true);

    ObjectCreate(ChartID(), objectBackgroundLabelName, OBJ_RECTANGLE_LABEL, 0, 0, 0);
    ObjectSetInteger(ChartID(), objectBackgroundLabelName, OBJPROP_XDISTANCE, 10);
    ObjectSetInteger(ChartID(), objectBackgroundLabelName, OBJPROP_YDISTANCE, y);
    ObjectSetInteger(ChartID(), objectBackgroundLabelName, OBJPROP_XSIZE, 200);
    ObjectSetInteger(ChartID(), objectBackgroundLabelName, OBJPROP_YSIZE, 25);
    ObjectSetInteger(ChartID(), objectBackgroundLabelName, OBJPROP_BGCOLOR, clrWhite);
    ObjectSetInteger(ChartID(), objectBackgroundLabelName, OBJPROP_BORDER_COLOR, clrBlack);
    ObjectSetInteger(ChartID(), objectBackgroundLabelName, OBJPROP_BORDER_TYPE, BORDER_FLAT);
    ObjectSetInteger(ChartID(), objectBackgroundLabelName, OBJPROP_WIDTH, 0);
    ObjectSetInteger(ChartID(), objectBackgroundLabelName, OBJPROP_BACK, false);
    ObjectSetInteger(ChartID(), objectBackgroundLabelName, OBJPROP_SELECTABLE, false);
    ObjectSetInteger(ChartID(), objectBackgroundLabelName, OBJPROP_HIDDEN, true);

    ObjectCreate(0, labelName, OBJ_LABEL, 0, 0, 0, 0, 0, 0, 0);
    ObjectSetInteger(0, labelName, OBJPROP_XDISTANCE, 20);
    ObjectSetInteger(0, labelName, OBJPROP_YDISTANCE, y);
    ObjectSetInteger(0, labelName, OBJPROP_CORNER, CORNER_LEFT_UPPER);
    ObjectSetInteger(0, labelName, OBJPROP_COLOR, clrBlack);
    ObjectSetInteger(0, labelName, OBJPROP_BORDER_COLOR, clrAqua);
    ObjectSetString(0, labelName, OBJPROP_TEXT, indicatorslist[i]);
    ObjectSetString(0, labelName, OBJPROP_FONT, "Arial");
    ObjectSetInteger(0, labelName, OBJPROP_FONTSIZE, 12);

    ObjectCreate(0, value, OBJ_LABEL, 0, 0, 0, 0, 0, 0, 0);
    ObjectSetInteger(0, value, OBJPROP_XDISTANCE, 230);
    ObjectSetInteger(0, value, OBJPROP_YDISTANCE, y);
    ObjectSetInteger(0, value, OBJPROP_CORNER, CORNER_LEFT_UPPER);
    ObjectSetInteger(0, value, OBJPROP_COLOR, clrBlack);
    ObjectSetInteger(0, value, OBJPROP_BORDER_COLOR, clrAqua);
    ObjectSetString(0, value, OBJPROP_FONT, "Arial");
    ObjectSetInteger(0, value, OBJPROP_FONTSIZE, 12);
    ObjectSetString(0, value, OBJPROP_TEXT, "No Trade");

  }
}

//+------------------------------------------------------------------+
//| Get signal text (Buy sell no trade)                                       |
//+------------------------------------------------------------------+
string resultsText(const double &open[],const double &close[],int indicatorIndex, int value, int index) {
   
    
  int preresults[12];
  getResults(preresults,index-1);
  
  if((preresults[indicatorIndex]==-1&&close[index-1]<open[index-1])||(preresults[indicatorIndex]==1&&close[index-1]>open[index-1])){
   correct[indicatorIndex]++;
   consCorrectCompter[indicatorIndex]++;
   consWrongCompter[indicatorIndex]=0;
  } else if((preresults[indicatorIndex]==-1&&close[index-1]>open[index-1])||(preresults[indicatorIndex]==1&&close[index-1]<open[index-1])){
   wrong[indicatorIndex]++;
   consWrongCompter[indicatorIndex]++;
   consCorrectCompter[indicatorIndex]=0;
  }
  
  
  bool changed= true;
  if(preresults[indicatorIndex]==value){changed=false;}
  string signal ="";
  if (value == 1) {
    signal = "Buy";
    ObjectSetInteger(ChartID(), indicatorslist[indicatorIndex] + "bg", OBJPROP_BGCOLOR, clrGreen);
  } else if (value == -1) {
    signal = "Sell";
    ObjectSetInteger(ChartID(), indicatorslist[indicatorIndex] + "bg", OBJPROP_BGCOLOR, clrOrange);
  } else {
    signal= "Wait";
    ObjectSetInteger(ChartID(), indicatorslist[indicatorIndex] + "bg", OBJPROP_BGCOLOR, clrWheat);
  }

  if (changed) {
    signal = signal + "*";
  }
  
  double successRate= 100*(double)correct[indicatorIndex]/(double)(MathMax(correct[indicatorIndex]+wrong[indicatorIndex],1));

  ObjectSetString(0, indicatorslist[indicatorIndex], OBJPROP_TEXT, signal+" | SuccRate="+ MathRound(successRate)+"%");

  return indicatorslist[indicatorIndex] + " : " + signal +" ("+ MathRound(successRate)+"%)\n";

}


void UpdatePowerStatus(const double &open[],const double &close[],int &results[],int index){

    double power=power_buffer[index];
    
    double prePower= power_buffer[index-1]; 
  
  if((prePower<0&&close[index-1]<open[index-1])||(prePower>0&&close[index-1]>open[index-1])){
   correct[12]++;
  } else if((prePower<0&&close[index-1]>open[index-1])||(prePower>0&&close[index-1]<open[index-1])){
   wrong[12]++;
  }
  
 
    string globalSignal;
   // double successRate =100*(double)powerCorrect/MathMax((double)(powerCorrect+powerWrong),1) ; 
    if(power > 0)  {
       globalSignal= "Buy";
       ObjectSetInteger(ChartID(), "PowerPg", OBJPROP_BGCOLOR, clrGreen);
    } 
    
    if(power < 0)  {
       globalSignal= "Sell";
       ObjectSetInteger(ChartID(), "PowerPg", OBJPROP_BGCOLOR, clrOrange);
    }
    
    if(power == 0)  {
       globalSignal= "No Trade ";
       ObjectSetInteger(ChartID(), "PowerPg", OBJPROP_BGCOLOR, clrWheat);
    }
    
    datetime dt = TimeCurrent();
    MqlDateTime tm; 
      TimeToStruct(dt,tm);
    string date=(string)tm.day+"-"+(string)tm.mon+"-" +(string)tm.year;
 
    ObjectSetString(0, "12 Indicators", OBJPROP_TEXT, "12 Indicators-V"+VERSION+" | "+Symbol()+" ("+ date+") ");
    double successRate= 100*(double)correct[12]/(double)(MathMax(correct[12]+wrong[12],1)); 
    
   // Print("Power succrate =  ",successRate," | number of mesures = ",correct[12]+wrong[12]);
    ObjectSetString(0, "Power", OBJPROP_TEXT, globalSignal+ " | Power = "+(string) MathRound(power)+"%| SuccRate = "+ MathRound(successRate)+"%");
}
