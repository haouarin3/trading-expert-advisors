//+------------------------------------------------------------------+
//|                                                  1HourExpert.mq5 |
//|                                               Haouari Noureddine |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Haouari Noureddine"
#property link      "https://t.me/joinchat/Mj8ZLBO9x56DZgN2vIwq7w"
#property version   "1.0.4"
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
#include <Trade\Trade.mqh>
#include <Trade\PositionInfo.mqh>

#define MA_MAGIC 75615487
int ExtHandle = 0;
bool ExtHedging = false;
CTrade ExtTrade;
CPositionInfo myposition;

input double MinPowerToOpen  = 99;
input double MinPowerToClose  = 99;
input double volume = 0.1;

input double STOPLOSS = 0;// STOPLOSS by position (PIPs)
input double TAKEPROFIT = 0;// Take profit by position (PIPs)
input int MaxNumberOfPositions = 0;

input double STOPLOSSTOTAL = 0.05;//STOPLOSSTOTAL % (0->1)
input double TAKEPROFITTOTAL = 0.1;//TAKEPROFITTOTAL % (0->1)
input double MinimumProfitToIncrease=0.05;
input ENUM_TIMEFRAMES Frame1=PERIOD_D1;
input ENUM_TIMEFRAMES Frame2=PERIOD_H1;
input double minAdx=25;

input bool WithPower =true; // With Power


input int max_period_number = 200;
input bool enhancedPowerCalculation = false;// 
input double historicalFactor= 0; 
input double instantaneousFactor= 1; 

input int MA1_Period = 10; // Moving Average period 01
input int MA2_Period = 30; // Moving Average period 02
input int MA3_Period = 30; // Moving Average period 03
input int MA4_Period = 40; // Moving Average period 04
input int MA5_Period = 50; // Moving Average period 05
input int MA6_Period = 60; // Moving Average period 06
input int MA7_Period = 70; // Moving Average period 07
input int MA8_Period = 80; // Moving Average period 08
input int MA9_Period = 90; // Moving Average period 09
input int MA10_Period = 100; // Moving Average period 10
input int MA11_Period = 150; // Moving Average period 11
input int MA12_Period = 200; // Moving Average period 12

int m_nLastBars=0;
int indicators12_handler=0;
int adx_handler=0;

int OnInit()
  {

    ExtHedging = ((ENUM_ACCOUNT_MARGIN_MODE) AccountInfoInteger(ACCOUNT_MARGIN_MODE) == ACCOUNT_MARGIN_MODE_RETAIL_HEDGING);
    ExtTrade.SetMarginMode();
    ExtTrade.SetTypeFillingBySymbol(Symbol());
//---
    indicators12_handler=iCustom(_Symbol, Frame1,"..\\Experts\\trading-expert-advisors\\12movingAverages",max_period_number, enhancedPowerCalculation, historicalFactor, instantaneousFactor,MA1_Period,MA2_Period,MA3_Period,MA4_Period,MA5_Period,MA6_Period,MA7_Period,MA8_Period,MA9_Period,MA10_Period,MA11_Period,MA12_Period);//12movingAverages,12indicators

    adx_handler = iADX(_Symbol, Frame1, 7);
//---
   return(INIT_SUCCEEDED);
  }
  
  
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---

ObjectsDeleteAll(0);
  }
  
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
  if (isThereANewCandel()) {
    CheckForOpen();
    CheckForClose();
    }
  }



//+------------------------------------------------------------------+
//| Close position by  indicatorIndex                              |
//+------------------------------------------------------------------+

void CheckForClose(){
  double power=getValue(12,1); 
  Print("Power = ",power);
        
  double balance = AccountInfoDouble(ACCOUNT_BALANCE);
  
    double adx[];
    double pdi[];
    double mdi[];
    if (CopyBuffer(adx_handler, 0, 0, 1, adx) != 1 || CopyBuffer(adx_handler, 1, 0, 1, pdi) != 1 || CopyBuffer(adx_handler, 2, 0, 1, mdi) != 1) {
       Print("CopyBuffer from ahc failed, no data");
       return;
    }
    
    
    
  if(adx[0]<minAdx || MathAbs(power)<MinPowerToClose || (TAKEPROFITTOTAL!=0&&GetTotalProfit(MA_MAGIC)>=TAKEPROFITTOTAL*balance) ||  (STOPLOSSTOTAL!=0&&GetTotalProfit(MA_MAGIC)<= -1*STOPLOSSTOTAL*balance)) {
   ClosePosition();
  }        
  
}


void CheckForOpen(){
    double adx[];
    double pdi[];
    double mdi[];
    if (CopyBuffer(adx_handler, 0, 0, 1, adx) != 1 || CopyBuffer(adx_handler, 1, 0, 1, pdi) != 1 || CopyBuffer(adx_handler, 2, 0, 1, mdi) != 1) {
       Print("CopyBuffer from ahc failed, no data");
       return;
    }
    
    
    
    
    MqlRates rt[2];
     if (CopyRates(_Symbol,Frame2, 0, 2, rt) != 2) {
       Print("CopyRates of ", _Symbol, " failed, no history");
       return;
     }
    ArraySetAsSeries(rt, true);
    
    double power=getValue(12,1);

    bool lastCandelAgainstTrend = ((rt[0].close>rt[0].open&&power<0) ||(rt[0].open>rt[0].close&&power>0));
   
      
   
    //Print("==> CheckForOpen ind=",indicatorslist[indicatorIndex]);
double balance = AccountInfoDouble(ACCOUNT_BALANCE);
     
    if ((adx[0]>minAdx)&&(NumberOfPositions()==0||GetTotalProfit(MA_MAGIC)>=MinimumProfitToIncrease*balance)&&(MaxNumberOfPositions==0 || NumberOfPositions()<=MaxNumberOfPositions)&&(MathAbs(power)>=MinPowerToOpen)&&lastCandelAgainstTrend) { // Result != 0 not wait
      
      int direction= WithPower?1:-1;
      if(power>0){
      OpenPositon(direction);
      } else if(power<0) {
      OpenPositon(-1*direction);
      }
    
    }
    
}






//+------------------------------------------------------------------+
//| Open position by  indicatorIndex with  sig = -1 sell , 1 buy                               |
//+------------------------------------------------------------------+
void OpenPositon(int sig) {
  
  MqlTick tick; // Structure to get the latest prices      
  SymbolInfoTick(Symbol(), tick); // Assign current prices to structure 


  ENUM_ORDER_TYPE signal = WRONG_VALUE;
     double tp=0;
     double sl=0;
     
 if (sig == 1) {
    signal = ORDER_TYPE_BUY; // buy conditions
    if(STOPLOSS!=0){
       sl = NormalizeDouble(tick.bid - STOPLOSS * _Point * 10,_Digits);
      } 
      
    if(TAKEPROFIT!=0){
       tp = NormalizeDouble(tick.bid + TAKEPROFIT * _Point* 10,_Digits);
      } 
  } else if (sig == -1) {
    signal = ORDER_TYPE_SELL; // sell conditions
   
   
    if(STOPLOSS!=0){
       sl = NormalizeDouble(tick.ask + STOPLOSS * _Point * 10,_Digits) ;
      } 
      
    if(TAKEPROFIT!=0){
       tp = NormalizeDouble(tick.ask - TAKEPROFIT * _Point * 10,_Digits) ;
      } 

  } else {
   return;
    Alert("Signal not considered");
  }
  
  long magicNumber=  MA_MAGIC;  
  ExtTrade.SetExpertMagicNumber(magicNumber);
  ExtTrade.PositionOpen(_Symbol, signal, volume,
  SymbolInfoDouble(_Symbol, signal == ORDER_TYPE_SELL ? SYMBOL_BID : SYMBOL_ASK),sl, tp,"1HourExpert");
  

  
  

  if (ExtTrade.ResultRetcode() == 10008 || ExtTrade.ResultRetcode() == 10009) //Request is completed or order placed
  {
   
   //Print("==> Open Indicator = ",indicatorslist[indicatorIndex]);
   
   // OpenPendingPositon(PendingOrderSignal, volume,price,tp,0,"ORDER_TYPE_BUY_STOP",indicatorIndex);
    
  } else {
    //Print("==> !OK ", "==> Open Indicator = ",indicatorslist[indicatorIndex], ", err = ",GetLastError());
    ResetLastError();
    return;
  }

}

void ClosePosition(){
 int magicNumber=  MA_MAGIC;  
 for(int i=PositionsTotal()-1; i>=0; i--)
    {
       ulong ticket=PositionGetTicket(i);
       PositionSelectByTicket(ticket);
       //Print("==> Magic number of closed deal=",PositionGetInteger(POSITION_MAGIC)," | indicatorIndex = ",indicatorIndex);
       if(PositionGetString(POSITION_SYMBOL)==Symbol()&&PositionGetInteger(POSITION_MAGIC)==magicNumber){
         // Print("=======> Close");  
          ExtTrade.PositionClose(ticket);   
         }
    }  
 
 /*for(int i=OrdersTotal()-1;i>=0;i--)
     {
      ulong ticket=OrderGetTicket(i);
      OrderSelect(ticket);

      if(OrderGetString(ORDER_SYMBOL)==Symbol()&&OrderGetInteger(ORDER_MAGIC)==magicNumber)
        { 
         
          ExtTrade.OrderDelete(ticket);
        }
     }*/ 
}


int NumberOfPositions()
{
  int c=0;
  int magicNumber=  MA_MAGIC  ;
  for(int i=PositionsTotal()-1; i>=0; i--)
    {
       ulong ticket=PositionGetTicket(i);
       PositionSelectByTicket(ticket);
        if(PositionGetString(POSITION_SYMBOL)==Symbol()&&PositionGetInteger(POSITION_MAGIC)==magicNumber){ 
            c++;
         }
    } 
  return(c);
}



//+------------------------------------------------------------------+
//| Get value from indicator buffer                                     |
//+------------------------------------------------------------------+
int getValue(int indicatorIndex,int start = 0){
        double  value[];
        if (CopyBuffer(indicators12_handler,indicatorIndex, 0, 3, value) != 3) {
          Print("CopyBuffer from indicators12 failed, no data");
          return 0;
        } 
        ArraySetAsSeries(value, true);
 return (int)value[start];
}


//+------------------------------------------------------------------+
//| isThereANewCandel ? True or False                                   |
//+------------------------------------------------------------------+
bool isThereANewCandel() {
  bool m_bNewBar = false;
  int nBars = Bars(Symbol(), Frame2);
  if (m_nLastBars != nBars) {
    m_nLastBars = nBars;
    m_bNewBar = true;
  }
  return m_bNewBar;
}


double GetTotalProfit(long Magic){
   double totalProfit = 0; 
   for(int i=PositionsTotal()-1; i>=0; i--)
    {
       ulong ticket=PositionGetTicket(i);
       PositionSelectByTicket(ticket);
       if(PositionGetString(POSITION_SYMBOL)==Symbol()&&PositionGetInteger(POSITION_MAGIC)==Magic){
          totalProfit+=PositionGetDouble(POSITION_PROFIT);
         }
    }  
   return totalProfit;
}
