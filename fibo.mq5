//+------------------------------------------------------------------+
//|                                                         fibo.mq5 |
//|                                               HAOUARI NOureddine |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "HAOUARI NOureddine"
#property link      "https://www.mql5.com"
#property version   "1.00"
//--- description 
#property description "Script draws \"Fibonacci Retracement\" graphical object." 
#property description "Anchor point coordinates are set in percentage of" 
#property description "the chart window size." 
//--- display window of the input parameters during the script's launch 
#property script_show_inputs 
//--- input parameters of the script 
input string          InpName="FiboLevels";      // Object name 
input color           InpColor=clrRed;           // Object color
input int NumberOfPeriods = 5;                            // Number of candles 
input ENUM_LINE_STYLE InpStyle=STYLE_DASHDOTDOT; // Line style 
input int             InpWidth=2;                // Line width 
input bool            InpBack=false;             // Background object 
input bool            InpSelection=true;         // Highlight to move 
input bool            InpRayLeft=false;          // Object's continuation to the left 
input bool            InpRayRight=false;         // Object's continuation to the right 
input bool            InpHidden=true;            // Hidden in the object list 
input long            InpZOrder=0;               // Priority for mouse click

void OnInit()
  {
  
    
  }
//+------------------------------------------------------------------+
//| deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
   ObjectsDeleteAll(0);
  }
     
//+------------------------------------------------------------------+ 
//| Create Fibonacci Retracement by the given coordinates            | 
//+------------------------------------------------------------------+ 
bool FiboLevelsCreate(const long            chart_ID=0,        // chart's ID 
                      const string          name="FiboLevels", // object name 
                      const int             sub_window=0,      // subwindow index  
                      datetime              time1=0,           // first point time 
                      double                price1=0,          // first point price 
                      datetime              time2=0,           // second point time 
                      double                price2=0,          // second point price 
                      const color           clr=clrRed,        // object color 
                      const ENUM_LINE_STYLE style=STYLE_SOLID, // object line style 
                      const int             width=1,           // object line width 
                      const bool            back=false,        // in the background 
                      const bool            selection=true,    // highlight to move 
                      const bool            ray_left=false,    // object's continuation to the left 
                      const bool            ray_right=false,   // object's continuation to the right 
                      const bool            hidden=true,       // hidden in the object list 
                      const long            z_order=0)         // priority for mouse click 
  { 
//--- set anchor points' coordinates if they are not set 
   ChangeFiboLevelsEmptyPoints(time1,price1,time2,price2); 
//--- reset the error value 
   ResetLastError(); 
//--- Create Fibonacci Retracement by the given coordinates 
   if(!ObjectCreate(chart_ID,name,OBJ_FIBO,sub_window,time1,price1,time2,price2)) 
     { 
      Print(__FUNCTION__, 
            ": failed to create \"Fibonacci Retracement\"! Error code = ",GetLastError()); 
      return(false); 
     } 
//--- set color 
   ObjectSetInteger(chart_ID,name,OBJPROP_COLOR,clr); 
//--- set line style 
   ObjectSetInteger(chart_ID,name,OBJPROP_STYLE,style); 
//--- set line width 
   ObjectSetInteger(chart_ID,name,OBJPROP_WIDTH,width); 
//--- display in the foreground (false) or background (true) 
   ObjectSetInteger(chart_ID,name,OBJPROP_BACK,back); 
//--- enable (true) or disable (false) the mode of highlighting the channel for moving 
//--- when creating a graphical object using ObjectCreate function, the object cannot be 
//--- highlighted and moved by default. Inside this method, selection parameter 
//--- is true by default making it possible to highlight and move the object 
   ObjectSetInteger(chart_ID,name,OBJPROP_SELECTABLE,selection); 
   ObjectSetInteger(chart_ID,name,OBJPROP_SELECTED,selection); 
//--- enable (true) or disable (false) the mode of continuation of the object's display to the left 
   ObjectSetInteger(chart_ID,name,OBJPROP_RAY_LEFT,ray_left); 
//--- enable (true) or disable (false) the mode of continuation of the object's display to the right 
   ObjectSetInteger(chart_ID,name,OBJPROP_RAY_RIGHT,ray_right); 
//--- hide (true) or display (false) graphical object name in the object list 
   ObjectSetInteger(chart_ID,name,OBJPROP_HIDDEN,hidden); 
//--- set the priority for receiving the event of a mouse click in the chart 
   ObjectSetInteger(chart_ID,name,OBJPROP_ZORDER,z_order); 
//--- successful execution 
   return(true); 
  } 
//+------------------------------------------------------------------+ 
//| Set number of levels and their parameters                        | 
//+------------------------------------------------------------------+ 
bool FiboLevelsSet(int             levels,            // number of level lines 
                   double          &values[],         // values of level lines 
                   color           &colors[],         // color of level lines 
                   ENUM_LINE_STYLE &styles[],         // style of level lines 
                   int             &widths[],         // width of level lines 
                   const long      chart_ID=0,        // chart's ID 
                   const string    name="FiboLevels") // object name 
  { 
//--- check array sizes 
   if(levels!=ArraySize(colors) || levels!=ArraySize(styles) || 
      levels!=ArraySize(widths) || levels!=ArraySize(widths)) 
     { 
      Print(__FUNCTION__,": array length does not correspond to the number of levels, error!"); 
      return(false); 
     } 
//--- set the number of levels 
   ObjectSetInteger(chart_ID,name,OBJPROP_LEVELS,levels); 
//--- set the properties of levels in the loop 
   for(int i=0;i<levels;i++) 
     { 
      //--- level value 
      ObjectSetDouble(chart_ID,name,OBJPROP_LEVELVALUE,i,values[i]); 
      //--- level color 
      ObjectSetInteger(chart_ID,name,OBJPROP_LEVELCOLOR,i,colors[i]); 
      //--- level style 
      ObjectSetInteger(chart_ID,name,OBJPROP_LEVELSTYLE,i,styles[i]); 
      //--- level width 
      ObjectSetInteger(chart_ID,name,OBJPROP_LEVELWIDTH,i,widths[i]); 
      //--- level description 
      ObjectSetString(chart_ID,name,OBJPROP_LEVELTEXT,i,DoubleToString(100*MathAbs(values[i]),1)); 
     } 
//--- successful execution 
   return(true); 
  } 
//+------------------------------------------------------------------+ 
//| Move Fibonacci Retracement anchor point                          | 
//+------------------------------------------------------------------+ 
bool FiboLevelsPointChange(const long   chart_ID=0,        // chart's ID 
                           const string name="FiboLevels", // object name 
                           const int    point_index=0,     // anchor point index 
                           datetime     time=0,            // anchor point time coordinate 
                           double       price=0)           // anchor point price coordinate 
  { 
//--- if point position is not set, move it to the current bar having Bid price 
   if(!time) 
      time=TimeCurrent(); 
   if(!price) 
      price=SymbolInfoDouble(Symbol(),SYMBOL_BID); 
//--- reset the error value 
   ResetLastError(); 
//--- move the anchor point 
   if(!ObjectMove(chart_ID,name,point_index,time,price)) 
     { 
      Print(__FUNCTION__, 
            ": failed to move the anchor point! Error code = ",GetLastError()); 
      return(false); 
     } 
//--- successful execution 
   return(true); 
  } 
//+------------------------------------------------------------------+ 
//| Delete Fibonacci Retracement                                     | 
//+------------------------------------------------------------------+ 
bool FiboLevelsDelete(const long   chart_ID=0,        // chart's ID 
                      const string name="FiboLevels") // object name 
  { 
//--- reset the error value 
   ResetLastError(); 
//--- delete the object 
   if(!ObjectDelete(chart_ID,name)) 
     { 
      Print(__FUNCTION__, 
            ": failed to delete \"Fibonacci Retracement\"! Error code = ",GetLastError()); 
      return(false); 
     } 
//--- successful execution 
   return(true); 
  } 
//+------------------------------------------------------------------+ 
//| Check the values of Fibonacci Retracement anchor points and set  | 
//| default values for empty ones                                    | 
//+------------------------------------------------------------------+ 
void ChangeFiboLevelsEmptyPoints(datetime &time1,double &price1, 
                                 datetime &time2,double &price2) 
  { 
//--- if the second point's time is not set, it will be on the current bar 
   if(!time2) 
      time2=TimeCurrent(); 
//--- if the second point's price is not set, it will have Bid value 
   if(!price2) 
      price2=SymbolInfoDouble(Symbol(),SYMBOL_BID); 
//--- if the first point's time is not set, it is located 9 bars left from the second one 
   if(!time1) 
     { 
      //--- array for receiving the open time of the last 10 bars 
      datetime temp[10]; 
      CopyTime(Symbol(),Period(),time2,10,temp); 
      //--- set the first point 9 bars left from the second one 
      time1=temp[0]; 
     } 
//--- if the first point's price is not set, move it 200 points below the second one 
   if(!price1) 
      price1=price2-200*SymbolInfoDouble(Symbol(),SYMBOL_POINT); 
  }   
 
  double max= 0;
  double min= 1000000; 
  int counter = 0; 
  int lastBar=0; 
  int lastBar0=0;
  //+------------------------------------------------------------------+
//| Accumulation/Distribution                                        |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
   
   
   

   int period = NumberOfPeriods;

   int i,limit;



//--- we can copy not all data
   int to_copy;
   if(prev_calculated>rates_total || prev_calculated<0) to_copy=rates_total;
   else
     {
      to_copy=rates_total-prev_calculated;
      if(prev_calculated>0) to_copy++;
     }
//---- get ma buffers
   if(IsStopped()) return(0); //Checking for stop flag
  
//--- first calculation or number of bars was changed
   if(prev_calculated<period)
      limit=period;
   else limit=prev_calculated-1;
//--- the main loop of calculations

 //--- check for bars count
 
  
   if(rates_total<period ){
      return(0);// not enough bars for calculation 
      }
  lastBar0=Bars(Symbol(), PERIOD_W1,time[0],time[limit]);
  int maxBars =Bars(Symbol(), PERIOD_W1);


   
   for(i=limit;i<rates_total-1 && !IsStopped();i++)
     {
     //--- fill the array of prices 
//--- create an object 
        //ObjectsDeleteAll(0);
        

  //if(MathMod(i,period )==0){
  if(isThereANewCandel(time[0],time[i],PERIOD_W1)) {
     string name = InpName+lastBar;
   if((lastBar<(maxBars-10)) || !FiboLevelsCreate(0,name,0,time[i-counter-1],max,time[i-1],min,InpColor, 
      InpStyle,InpWidth,InpBack,InpSelection,InpRayLeft,InpRayRight,InpHidden,InpZOrder)) 
     { ;
      //return false; 
     } else {
   //--- redraw the chart and wait for 1 second 
   AddLevels(name);
   double Range = MathRound((max-min)*10000); 
   double RangePrice =  min;
   int midPeriod=i- MathRound((double (counter))/2.0)-1;
   double RangeTime = time[midPeriod];
   TextCreate(0,"RangeMin"+lastBar,0,RangeTime,RangePrice,Range,"Arial",14,clrRed);
   TextCreate(0,"RangeMax"+lastBar,0,time[midPeriod],max+0.0020,"["+lastBar+"/"+maxBars+"]","Arial",12,clrRed);
   ChartRedraw(); 
   max = 0; 
   min = 1000000;
   counter=0;
   }
  }else if(Bars(_Symbol,PERIOD_W1)==Bars(_Symbol,PERIOD_W1,time[0],time[i])){
      string name = InpName+(lastBar+1);
         if(FiboLevelsCreate(0, name,0,time[i-counter-1],max,time[i-1],min,InpColor, 
      InpStyle,InpWidth,InpBack,InpSelection,InpRayLeft,InpRayRight,InpHidden,InpZOrder)){
            FiboLevelsDelete(name);
            ObjectDelete(0,"RangeMin"+(lastBar+1));
            ObjectDelete(0,"RangeMax"+(lastBar+1));
            AddLevels(name);
            double Range = MathRound((max-min)*10000); 
            double RangePrice =  min;
            int midPeriod=i- MathRound((double (counter))/2.0)-1;
            double RangeTime = time[midPeriod];
            TextCreate(0,"RangeMin"+lastBar,0,RangeTime,RangePrice,Range,"Arial",14,clrRed);
            TextCreate(0,"RangeMax"+lastBar,0,time[midPeriod],max+0.0020,"["+lastBar+"/"+maxBars+"]","Arial",12,clrRed);
            ChartRedraw(); 
         }
  }
  

   max = MathMax(max,high[i]);
   min = MathMin(min,low[i]);
   counter++;
   //--- return value of prev_calculated for next call
   }
   

   return(rates_total);   
   }
   void AddLevels(string Name){
   
      double PercLevels[59]={-0.236,-0.382,-0.50,-0.618,-0.786,-1.00,-1.236,-1.382,-1.5,-1.618,-1.786,-2,-2.236,-2.382,-2.5,-2.618,-2.786,-3,-3.236,-3.382,-3.5,-3.618,-3.786,-4,-4.236,-4.382,-4.5,-4.618,-4.786,0,0.236,0.382,0.50,0.618,0.786,1.00,1.236,1.382,1.5,1.618,1.786,2,2.236,2.382,2.5,2.618,2.786,3,3.236,3.382,3.5,3.618,3.786,4,4.236,4.382,4.5,4.618,4.786};
      color Colors[59]={clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow,clrYellow};
      ENUM_LINE_STYLE LineStyles[59]={STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID,STYLE_SOLID};
       Colors[29]=clrRed;
       Colors[35]=clrRed;
      
      int Widths[59]= {1,1,1,1,1,4,1,1,1,1,1,4,1,1,1,1,1,4,1,1,1,1,1,4,1,1,1,1,1,4,1,1,1,1,1,4,1,1,1,1,1,4,1,1,1,1,1,4,1,1,1,1,1,4,1,1,1,1,1};
      
      FiboLevelsSet(59,PercLevels,Colors,LineStyles,Widths,0,Name);
   }
   
   
   bool TextCreate(const long              chart_ID=0,               // chart's ID
                const string            name="Text",              // object name
                const int               sub_window=0,             // subwindow index
                datetime                time=0,                   // anchor point time
                double                  price=0,                  // anchor point price
                const string            text="Text",              // the text itself
                const string            font="Arial",             // font
                const int               font_size=10,             // font size
                const color             clr=clrRed,               // color
                const double            angle=0.0,                // text slope
                const ENUM_ANCHOR_POINT anchor=ANCHOR_LEFT_UPPER, // anchor type
                const bool              back=false,               // in the background
                const bool              selection=false,          // highlight to move
                const bool              hidden=true,              // hidden in the object list
                const long              z_order=0)                // priority for mouse click
  {
//--- set anchor point coordinates if they are not set
   ChangeTextEmptyPoint(time,price);
//--- reset the error value
   ResetLastError();
//--- create Text object
   if(!ObjectCreate(chart_ID,name,OBJ_TEXT,sub_window,time,price))
     {
      Print(__FUNCTION__,
            ": failed to create \"Text\" object! Error code = ",GetLastError());
      return(false);
     }
//--- set the text
   ObjectSetString(chart_ID,name,OBJPROP_TEXT,text);
//--- set text font
   ObjectSetString(chart_ID,name,OBJPROP_FONT,font);
//--- set font size
   ObjectSetInteger(chart_ID,name,OBJPROP_FONTSIZE,font_size);
//--- set the slope angle of the text
   ObjectSetDouble(chart_ID,name,OBJPROP_ANGLE,angle);
//--- set anchor type
   ObjectSetInteger(chart_ID,name,OBJPROP_ANCHOR,anchor);
//--- set color
   ObjectSetInteger(chart_ID,name,OBJPROP_COLOR,clr);
//--- display in the foreground (false) or background (true)
   ObjectSetInteger(chart_ID,name,OBJPROP_BACK,back);
//--- enable (true) or disable (false) the mode of moving the object by mouse
   ObjectSetInteger(chart_ID,name,OBJPROP_SELECTABLE,selection);
   ObjectSetInteger(chart_ID,name,OBJPROP_SELECTED,selection);
//--- hide (true) or display (false) graphical object name in the object list
   ObjectSetInteger(chart_ID,name,OBJPROP_HIDDEN,hidden);
//--- set the priority for receiving the event of a mouse click in the chart
   ObjectSetInteger(chart_ID,name,OBJPROP_ZORDER,z_order);
//--- successful execution
   return(true);
  }
  
  //+------------------------------------------------------------------+
//| Check anchor point values and set default values                 |
//| for empty ones                                                   |
//+------------------------------------------------------------------+
void ChangeTextEmptyPoint(datetime &time,double &price)
  {
//--- if the point's time is not set, it will be on the current bar
   if(!time)
      time=TimeCurrent();
//--- if the point's price is not set, it will have Bid value
   if(!price)
      price=SymbolInfoDouble(Symbol(),SYMBOL_BID);
  }
  
  
bool isThereANewCandel(datetime date1,datetime date2,ENUM_TIMEFRAMES period=PERIOD_CURRENT) {
  bool m_bNewBar = false;
  int nBars = Bars(Symbol(), period,date1,date2);
  
  if (nBars>lastBar) {
    m_bNewBar = true;
    lastBar = nBars;
  }
  
  return m_bNewBar;
}